//toastr 顯示右下
toastr_success.onHidden = function() {
   location.href = 'home';
    $('#login_btn').text('登入');
};
toastr_error.onHidden = function() {
    $('#login_btn').text('登入');
};
var frm_01 = $('#frm_01'); // 表單一
var frm_01_FormData ;
    $(document).ready(function() {
    $('#login_btn').click(function(){
        frm_01.parsley().whenValidate({
        }).done(function() {
            $('#login_btn').text('登入中...');
            $('#login_btn').attr('disabled','disabled');
                frm_01_FormData = new FormData(frm_01[0]);
                axios({
                    method: 'post',
                    baseURL: base_url + cms_path + "login/check",
                    headers: {'Content-Type': 'multipart/form-data'},
                    data: frm_01_FormData
                }).then(function (response) {
                    $('#login_btn').attr('disabled', 'disabled');
                    //handle success
                    if (response.data.result == 'success') {
                        toastr.success('登入成功', '', toastr_success);
                    } else {
                        $('#login_btn').text('登入');
                        $('#login_btn').removeAttr('disabled');
                        $('#account').val('');
                        $('#password').val('');
                        toastr.error('帳號或密碼有誤!!', '', toastr_error);
                    }
                }).catch(function (response) {
                    $('#login_btn').text('登入');
                    // $('#login_btn').removeAttr('disabled');
                    $('#account').val('');
                    $('#password').val('');
                    toastr.error('帳號或密碼有誤!!', '', toastr_error);
                });
        });

    });

});