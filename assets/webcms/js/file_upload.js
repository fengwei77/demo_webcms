//loading over-lay
var customElement   = $("<div>", {
    id      : "countdown",
    css     : {
        "font-size" : "50px",
        "padding-top" : "120px",
        "padding-left" : "-10px",
        "font-size" : "24px",
        "font-weight" : "500",
        "font-family":"Microsoft JhengHei"
    },
     text    : '處理中'
});
$(function(){
    $('.upload_files').change(function(){
         if ( this.value == ''){
             return false;
         }
         // readURL(this);
        // var path, url;
        url = this.value;
        url = url.split("\\");
        path = url[2];
        var allowedExtensions = /(\.pdf|\.zip)$/i;
         if(!allowedExtensions.exec(path)){
            alert('只能上傳副檔名為 .pdf/.zip 的檔案.');
            fileInput.value = '';
            return false;
        }

        _file_id = $(this).attr('file_id');
        bootbox.confirm({
            message: "<span style='font-size: 1.3em;\n" +
            "    font-weight: 600;'>確定要上傳此檔案?</span>",
            buttons: {
                confirm: {
                    label: '確定',
                    className: 'btn-primary'
                },
                cancel: {
                    label: '取消',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if(result){
                    $.LoadingOverlay("show", {
                        custom  : customElement
                    });

                    var file_data = $('#upload_files_'+_file_id).prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('files', file_data);
                    $.ajax(base_url+ cms_path + 'ajax_upload/upload_single_file', {
                        method: "POST",
                        data: form_data,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success: function (response) {
                            setTimeout(function(){
                                $.LoadingOverlay("hide");
                            }, 800);
                            //按鈕控制
                            $('#file_name_view').html('已上傳檔案：<br><a href="'+response.file_path+'" target="_blank" > ' + path  +'</a>');
                            $('#upload_btn_'+ _file_id).hide();
                            $('#del_upload_'+ _file_id).show();

                            $.fancybox.close( );
                        },
                        error: function () {
                            console.log('Upload error');
                        }
                    });
                }
            }
        });
    });




    $('.del_upload').click(function(){
        _file_id = $(this).attr('file_id');
        $('#file_name_view').html('');


        $('#upload_btn_' + _file_id).show();
        $('#del_upload_' + _file_id).hide();
    });

});




