var el =  $('#crop_img');
var temp_file_id ='';
//loading over-lay
var customElement   = $("<div>", {
    id      : "countdown",
    css     : {
        "font-size" : "50px",
        "padding-top" : "120px",
        "padding-left" : "-10px",
        "font-size" : "24px",
        "font-weight" : "500",
        "font-family":"Microsoft JhengHei"
    },
     text    : '處理中'
});
$(function(){
    $('.upload_files').change(function(){
         if ( this.value == ''){
             return false;
         }
        _file_id = $(this).attr('file_id');
        bootbox.confirm({
            message: "<span style='font-size: 1.3em;\n" +
            "    font-weight: 600;'>確定使用這張圖片?</span>",
            buttons: {
                confirm: {
                    label: '確定',
                    className: 'btn-primary'
                },
                cancel: {
                    label: '取消',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if(result){
                    $.LoadingOverlay("show", {
                        custom  : customElement
                    });

                    var file_data = $('#upload_files_'+_file_id).prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('files', file_data);
                    form_data.append($('#csrf').attr('name'), $('#csrf').val());

                    $.ajax(base_url+ cms_path + 'ajax_upload/upload_file', {
                        method: "POST",
                        data: form_data,
                        processData: false,
                        contentType: false,
                        dataType: 'json',
                        success: function (response) {
                            setTimeout(function(){
                                $.LoadingOverlay("hide");
                            }, 500);
                            if(response.error != undefined) {
                                if (response.error.indexOf("超出PHP限制最大") > 0) {
                                    alert('檔案過大請重新選擇');
                                    $('#preview_upload_pic_' + _file_id).attr('src', '');
                                    $('#preview_fancybox_' + _file_id).attr('href', '');
                                    $('#crop_img').attr('src', '');
                                    $('#upload_pic_btn_' + _file_id).show();
                                    $('#file_cropper_' + _file_id).hide();
                                    $('#del_upload_pic_' + _file_id).hide();
                                    for (var i = 0; i < files_array.length; i++) {
                                        var temp = $.each(files_array[i], function (key, value) {
                                            if (key == _file_id) {
                                                // files_array.splice(i, 1);
                                                value.file_path = '';
                                                return false;
                                            }
                                        });
                                    }
                                    return false;
                                }
                            }
                            // console.log(response.error);
                            // alert(response.error);
                            $('#preview_upload_pic_' + _file_id).attr('src', base_url + response.file);
                            $('#preview_fancybox_' + _file_id).attr('href', base_url + response.file);

                            for(var i = 0 ; i<  files_array.length; i++){

                                var temp =  $.each(files_array[i], function(key, value) {
                                    if(key == _file_id){
                                        files_array.splice(i, 1);
                                        return false;
                                    }
                                });
                            }

                            var obj = {};
                            var obj_content = {};
                            // obj_content['uuid'] = '';
                            // obj_content['desc'] = '';
                            obj_content[ 'file_path'] = response.file;


                            obj[_file_id] = obj_content;
                             // files_array.push(obj);
                            files_array[(_file_id)] = obj;
                             console.log(files_array);

                            //按鈕控制
                            $('#upload_pic_btn_'+ _file_id).hide();
                            $('#file_cropper_'+ _file_id).show();
                            $('#del_upload_pic_'+ _file_id).show();

                            $.fancybox.close( );
                        },
                        error: function () {
                            console.log('Upload error');
                        }
                    });
                }else{
                    console.log('cancel');
                    $('#upload_files_'+ _file_id).val('');
                    $('#preview_upload_pic_'+ _file_id).attr('src','');
                    $('#preview_fancybox_' + _file_id).attr('href', '');
                    $('#crop_img').attr('src', '');
                    $('#upload_pic_btn_' + _file_id).show();
                    $('#file_cropper_'  + _file_id).hide();
                    $('#del_upload_pic_' + _file_id).hide();
                    for(var i = 0 ; i<  files_array.length; i++){
                        var temp =  $.each(files_array[i], function(key, value) {
                            if(key == _file_id){
                                // files_array.splice(i, 1);
                                value.file_path =  '';
                                return false;
                            }
                        });
                    }

                }
            }
        });
    });

    $('.preview_fancybox').fancybox();
    // $('.fancybox').fancybox();

    $('.basic-result').on('click', function() {

        if( output_cropper_wh.split(',')[0] == 0 &&  output_cropper_wh.split(',')[1] == 0){
            el.cropper('getCroppedCanvas').toBlob(function (blob) {
                var form_data = new FormData();
                form_data.append($('#csrf').attr('name'), $('#csrf').val());
                form_data.append('croppedImage', blob);

                $.ajax(base_url+ cms_path + 'ajax_upload/cropper', {
                    method: "POST",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function (response) {
                        console.log(response.file);
                        $('#preview_upload_pic_' + preview_temp_id).attr('src', base_url + response.file);
                        $('#preview_fancybox_'+ preview_temp_id).attr('href', base_url + response.file);
                        for(var i = 0 ; i<  files_array.length; i++){
                            var temp =  $.each(files_array[i], function(key, value) {
                                if(key == temp_file_id){
                                    value.file_path =   response.file;
                                    return false;
                                }
                            });
                        }
                        $.fancybox.close( );
                        el.cropper('destroy');
                    },
                    error: function () {
                        console.log('Upload error');
                    }
                });
            });
        }else{

            el.cropper(
                'getCroppedCanvas',
                {
                    width:  output_cropper_wh.split(',')[0],
                    height:  output_cropper_wh.split(',')[1],
                }).toBlob(function (blob) {
                var form_data = new FormData();
                form_data.append($('#csrf').attr('name'), $('#csrf').val());
                form_data.append('croppedImage', blob);
                $.ajax(base_url+ cms_path + 'ajax_upload/cropper', {
                    method: "POST",
                    data: form_data,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function (response) {
                        console.log(response.file);
                        $('#preview_upload_pic_' + preview_temp_id).attr('src', base_url + response.file);
                        $('#preview_fancybox_'+ preview_temp_id).attr('href', base_url + response.file);
                        for(var i = 0 ; i<  files_array.length; i++){
                            var temp =  $.each(files_array[i], function(key, value) {
                                if(key == temp_file_id){
                                    value.file_path =   response.file;
                                    return false;
                                }
                            });
                        }
                        $.fancybox.close( );
                        el.cropper('destroy');
                    },
                    error: function () {
                        console.log('Upload error');
                    }
                });
            });
        }

    });

    $(".cropper_open").fancybox({
        maxWidth	: '80%',
        maxHeight	: '80%',
        minWidth	: '30%',
        minHeight	: '30%',
        fitToView	: true,
        width		: '1000px',
        height		: '800px',
        autoSize	:  true,
        scrolling : 'auto' ,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',
        closeBtn : true,
        modal:true
    });

    $('.del_upload_pic').click(function(){
        _file_id = $(this).attr('file_id');
        console.log(_file_id);
        $('#upload_files_'+ _file_id).val('');
        $('#preview_upload_pic_'+ _file_id).attr('src','');
        $('#preview_fancybox_' + _file_id).attr('href', '');
        $('#crop_img').attr('src', '');
        $('#upload_pic_btn_' + _file_id).show();
        $('#file_cropper_'  + _file_id).hide();
        $('#del_upload_pic_' + _file_id).hide();
        //

        for(var i = 0 ; i<  files_array.length; i++){
            var temp =  $.each(files_array[i], function(key, value) {
                if(key == _file_id){
                    // files_array.splice(i, 1);
                    value.file_path =  '';
                    return false;
                }
            });
        }
        console.log(files_array);
    });

});

//裁圖片
var preview_temp_id='';
var output_cropper_wh = '';
function cropper_fire(id , ratio){

    temp_file_id = id;
    // alert(ratio.split(',')[0]);
    output_cropper_wh = ratio;
    preview_temp_id = id;
    if(ratio.split(',')[0] == 0){
        act_radio = 0;
    }else{
        act_radio = ratio.split(',')[0]/ratio.split(',')[1];
    }

    $('#crop_img').attr('src', $('#preview_upload_pic_' + id).attr('src'));
    $('.cropper_open').trigger('click');
    el.cropper({
        viewMode:2,
        aspectRatio: act_radio,
        autoCropArea: 1,
        cropBoxMovable: true,
        cropBoxResizable: true,
        dragMode: 'move',
        movable:true,
        responsive:true,
        preview: '.img-preview',
        crop: function(e) {
            // Output the result data for cropping image.
            console.log(e.x);
            console.log(e.y);
            console.log(e.width);
            console.log(e.height);
            console.log(e.rotate);
            console.log(e.scaleX);
            console.log(e.scaleY);
        }
    });
    $('#crop_img').show();
    $('.cancel_crop').show();
    $('.basic-result').show();

}



function  cancel_crop(){
    $('.cancel_crop').hide();
    $('.basic-result').hide();
    el.cropper('destroy');
    $('#crop_img').hide();

    $.fancybox.close( );
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#crop_img').attr('src', e.target.result);

        }


        reader.readAsDataURL(input.files[0]);
    }
}

