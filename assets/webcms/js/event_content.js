var send_add_url = base_url + cms_path + 'event_content/add_item';
var send_update_url = base_url + cms_path + 'event_content/update_item';
var frm_01 = $('#frm_01'); // 表單一
var frm_01_FormData ;

var toastr_success = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "800",
    "timeOut": "1000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
var toastr_error = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "800",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
$(document).ready(function() {
    //內容編輯
    _external_filemanager_file = "../bower_components/filemanager/";
    var segments = location.pathname.split( '/' );
    tinymce.init({
        language : 'zh_TW',
        selector: '#content_1',
        height: 300,
        menubar: false,
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : 'div',
        toolbar_items_size: 'normal',
        paste_as_text: true,
        image_advtab: true,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help wordcount'
        ],
        toolbar: '  undo redo | bold italic backcolor image | alignleft aligncenter alignright alignjustify | link |bullist numlist outdent indent | removeformat | code',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'],
        image_advtab: true,
        //upload plugin
        external_filemanager_path: _external_filemanager_file,
        filemanager_title:"檔案管理" ,
        external_plugins: { "filemanager" : "../../bower_components/filemanager/plugin.min.js"}

    });

    $.LoadingOverlaySetup(set_loading_bar);
    toastr_success.onHidden = function() {
        location.href = base_url + cms_path +  'events';
    };
    toastr_error.onHidden = function() {
        $(this).removeAttr('disabled');
        $('#send_form_btn').fadeIn();
    };
    //checkbox switch效果
    $('.item_status').bootstrapSwitch({
        onColor: 'success',
        offColor: 'default',
        onText: '啟用',
        offText: '關閉',
        onSwitchChange: function (event, state) {
            return;
        }
    });
    //checkbox switch效果
    $('.is_link').bootstrapSwitch({
        onColor: 'success',
        offColor: 'default',
        onText: '啟用',
        offText: '關閉',
        onSwitchChange: function (event, state) {
            return;
        }
    });


    //日期available time 及
    var now_time = moment();
    $('#bs_start_date').datetimepicker({
        defaultDate: moment().format("YYYY-MM-DD HH:mm"),
        locale: 'zh-tw',
        format: 'YYYY-MM-DD',
        sideBySide: true,
    }).on("dp.change", function (e) {
        if(moment(moment(e.date).format("YYYY-MM-DD HH:mm")).isAfter($('#bs_expire_date').data('date'))){
            toastr.error('上架日期不可超過下架日期', '錯誤' , toastr_error);
            $('#bs_start_date').data("DateTimePicker").date(moment($('#bs_expire_date').data('date')).add(-1, 'days').format("YYYY-MM-DD" + " 00:00"));
            return false;
        }
    });

    $('#bs_expire_date').datetimepicker({
        defaultDate:  moment().add(1, 'year').format("YYYY-MM-DD" + " 23:59"),
        locale: 'zh-tw',
        format: 'YYYY-MM-DD',
        sideBySide: true,
    }).on("dp.change", function (e) {
        console.log(moment(moment(e.date).format("YYYY-MM-DD HH:mm")).isBefore($('#bs_start_date').data('date')));
        if(moment(moment(e.date).format("YYYY-MM-DD HH:mm")).isBefore($('#bs_start_date').data('date'))){
            $('#bs_expire_date').data("DateTimePicker").date(moment($('#bs_start_date').data('date')).format("YYYY-MM-DD" + " 23:59"));
            toastr.error('下架日期不可在上架日期之前', '錯誤' , toastr_error);
            return false;
        }
    });



    var error_open = 0;

    send_url = send_add_url;
    if($('#uuid').val().length == 32 ){
        is_modify = true;
        send_url = send_update_url;
    }

    //送出表單
    $('#send_form_btn').click(function(){
        frm_01.parsley().whenValidate({
        }).done(function() {
            $('#send_form_btn').attr('disabled','disabled');
            $.LoadingOverlay("show");
             frm_01_FormData = new FormData(frm_01[0]);
            //照片說明放進去x
            for(var i = 0 ; i<  files_array.length; i++){
                var temp =  $.each(files_array[i], function(key, value) {
                    frm_01_FormData.append('file_' + (i+1), value.file_path);
                });
            }
            frm_01_FormData.append('files', JSON.stringify( files_array) );
            var content_1 = tinyMCE.get('content_1').getContent(); // get the content
            frm_01_FormData.append('content_1', htmlEncode(content_1));
            axios({
                method: 'post',
                baseURL: send_url ,
                headers: {'Content-Type': 'multipart/form-data' },
                data: frm_01_FormData
            })
                .then(function (response) {
                    //handle success
                    $.LoadingOverlay("hide");
                    if(response.data.result == 'success'){
                        toastr.success('', '完成' , toastr_success);
                    }else if(response.data.result == 'double'){
                        toastr.error('名稱不可以重複', '錯誤' , toastr_error);
                        $('#send_form_btn').removeAttr('disabled');
                    }else{
                        toastr.error('資料有誤', '' , toastr_error);
                        $('#send_form_btn').removeAttr('disabled');
                    }

                })
                .catch(function (response) {
                    toastr.error('資料有誤', '' , toastr_error);
                    $('#send_form_btn').removeAttr('disabled');
                });
        });

    });


});



function htmlEncode(value){
    return $('<div/>').text(value).html();
}

function htmlDecode(value){
    return $('<div/>').html(value).text();
}
