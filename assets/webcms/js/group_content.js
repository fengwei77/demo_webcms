var send_add_url = base_url + cms_path + 'group_content/add_item';
var send_update_url = base_url + cms_path + 'group_content/update_item';
var frm_01 = $('#frm_01'); // 表單一
var frm_01_FormData ;
var toastr_success = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "800",
    "timeOut": "1000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
var toastr_error = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "800",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
$(document).ready(function() {
    $.LoadingOverlaySetup(set_loading_bar);
    toastr_success.onHidden = function() {
        location.href = base_url + cms_path +  'user_groups';
    };
    toastr_error.onHidden = function() {
        $(this).removeAttr('disabled');
        $('#send_form_btn').fadeIn();
    };
    //checkbox switch效果
    $('.item_status').bootstrapSwitch({
        onColor: 'success',
        offColor: 'default',
        onText: '啟用',
        offText: '關閉',
        onSwitchChange: function (event, state) {
            return;
        }
    })

    // bind form using ajaxForm
    send_url = send_add_url;
    if($('#uuid').val().length == 32 ){
        send_url = send_update_url;
    }
    $('#send_form_btn').click(function(){
        frm_01.parsley().whenValidate({
        }).done(function() {
            $('#send_form_btn').attr('disabled','disabled');
            $.LoadingOverlay("show");
            var roles = GetCheckedValue('roles');
            frm_01_FormData = new FormData(frm_01[0]);
            frm_01_FormData.append('roles_arr', roles);
            axios({
                method: 'post',
                baseURL: send_url ,
                headers: {'Content-Type': 'multipart/form-data' },
                data: frm_01_FormData
            })
                .then(function (response) {
                    //handle success
                    $.LoadingOverlay("hide");
                    if(response.data.result == 'success'){
                        toastr.success('', '完成' , toastr_success);
                    }else if(response.data.result == 'double'){
                        toastr.error('名稱不可以重複', '錯誤' , toastr_error);
                        $('#send_form_btn').removeAttr('disabled');
                    }else{
                        toastr.error('資料有誤', '' , toastr_error);
                        $('#send_form_btn').removeAttr('disabled');
                    }

                })
                .catch(function (response) {
                    toastr.error('資料有誤', '' , toastr_error);
                    $('#send_form_btn').removeAttr('disabled');
                });
        });

    });






});



function GetCheckedValue(checkBoxName)
{
    return $('input:checkbox[name=' + checkBoxName + ']:checked').map(function ()
    {
        return $(this).val();
    }).get().join(',');
}