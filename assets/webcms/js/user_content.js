var send_add_url = base_url + cms_path + 'user_content/add_item';
var send_update_url = base_url + cms_path + 'user_content/update_item';
var frm_01 = $('#frm_01'); // 表單一
var frm_01_FormData ;
var toastr_success = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "800",
    "timeOut": "1000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
var toastr_error = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "800",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
$(document).ready(function() {
    $.LoadingOverlaySetup(set_loading_bar);
    toastr_success.onHidden = function() {
       location.href = base_url + cms_path +  'users';
    };
    toastr_error.onHidden = function() {
        $(this).removeAttr('disabled');
        $('#send_form_btn').fadeIn();
    };
    //checkbox switch效果
    $('.item_status').bootstrapSwitch({
        onColor: 'success',
        offColor: 'default',
        onText: '啟用',
        offText: '關閉',
        onSwitchChange: function (event, state) {
            return;
        }
    })


    //單元該群組權限內容
    var is_modify = false;
    $('#group').change(function(){

        // alert($('#group option:selected').attr('has_roles'));
        if(is_modify ){
            $('.checkbox-success').find('input[type="checkbox"]').prop('checked', false);
        }

        $('#table-msg').fadeOut();
        if($('#group option:selected').attr('has_roles') == ''){
            $('#table-msg').fadeIn();
            arr_id = '';
        }else{
            arr_id = $('#group option:selected').attr('has_roles').split(",");
        }
        $('.all_roles').fadeOut(300, function() {
            setTimeout(
                function(){
                    for (i = 0; i < arr_id.length; i++) {
                        $('.roles'+arr_id[i]).fadeIn();
                    }
                    if($("#group  option:selected").val() == $('#roles_uuid').val() ){
                        $("input[name='access_range[]']").each(function() {
                            access_range_arr = $('#access_range_str').val().split(",");
                            if(access_range_arr.indexOf($(this).val())>-1){
                                $(this).prop("checked", true);

                            }
                        });
                    }
                }
                ,500);
        });

        is_modify = true;
    });


    // bind form using ajaxForm
    send_url = send_add_url;
    if($('#uuid').val().length == 32 ){
        send_url = send_update_url;
        is_modify = true;
        $('#table-msg').fadeOut(100,function(){
            roles_arr = $('#roles').val().split(",");
            for (i = 0; i < roles_arr.length; i++) {
                $('.roles'+roles_arr[i]).fadeIn();
            }
        });

    }
    $('#send_form_btn').click(function(){
        frm_01.parsley().whenValidate({
        }).done(function() {
            $('#send_form_btn').attr('disabled','disabled');
            $.LoadingOverlay("show");
            var roles = GetCheckedValue('roles');
            frm_01_FormData = new FormData(frm_01[0]);
            frm_01_FormData.append('roles_arr', roles);

            axios({
                method: 'post',
                baseURL: send_url ,
                headers: {'Content-Type': 'multipart/form-data' },
                data: frm_01_FormData
            })
                .then(function (response) {
                    //handle success
                    $.LoadingOverlay("hide");
                    if(response.data.result == 'success'){
                        toastr.success('', '完成' , toastr_success);
                    }else if(response.data.result == 'double'){
                        toastr.error('信箱不可以重複', '錯誤' , toastr_error);
                        $('#send_form_btn').removeAttr('disabled');
                    }else{
                        toastr.error('資料有誤', '' , toastr_error);
                        $('#send_form_btn').removeAttr('disabled');
                    }

                })
                .catch(function (response) {
                    toastr.error('資料有誤', '' , toastr_error);
                    $('#send_form_btn').removeAttr('disabled');
                });
        });

    });






});



function GetCheckedValue(checkBoxName)
{
    return $('input:checkbox[name=' + checkBoxName + ']:checked').map(function ()
    {
        return $(this).val();
    }).get().join(',');
}