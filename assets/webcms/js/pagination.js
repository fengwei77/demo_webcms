var page_size = 20;
var form_data = [];
var frm_01 = $('#frm_01'); // 表單一
var frm_01_FormData ;

var customElement   = $("<div>", {
    id      : "countdown",
    css     : {
        "font-size" : "50px",
        "padding-top" : "120px",
        "padding-left" : "-10px",
        "font-size" : "24px",
        "font-weight" : "500",
        "font-family":"Microsoft JhengHei"
    },
    text    : '處理中...'
});

$(function(){

    page_name = getBaseName(window.location.href);
    page_size_select = $('#page_size_select');
    if( page_size_select.val() > 0){
        setTimeout(function(){
        }, page_size_select.val()*0);

        $('#data_msg').hide();
        $('#data_body').show();

    }
    page_size_select.change(function(){
        // alert(change_ps_url);
        form_data = [];
        form_data.push({ name: "page_size_name", value: page_name });
        form_data.push({ name: "page_size", value:  page_size_select.val() });

    });

});


function showResponse_com(responseText, statusText, xhr, $form)  {
    if(responseText == 'success'){
        location.reload();
    }
}
function getBaseName(url) {
    if(!url || (url && url.length === 0)) {
        return "";
    }
    var index = url.lastIndexOf("/") + 1;
    var filenameWithExtension = url.substr(index);
    var basename = filenameWithExtension.split(/[.?&#]+/)[0];

    // Handle '/mypage/' type paths
    if(basename.length === 0) {
        url = url.substr(0,index-1);
        basename = getBaseName(url);
    }
    return basename ? basename : "";
}