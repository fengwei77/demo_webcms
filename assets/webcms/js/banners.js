var send_status_url = base_url + cms_path + 'banners/change_item_status';
var send_change_order_url = base_url + cms_path + 'page_size/sort_func'
var frm_01 = $('#frm_01'); // 表單一
var frm_01_FormData ;
var toastr_success = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "800",
    "timeOut": "2000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
var action = 'delete';
var tmp_btn ;
$(function(){
    //縮圖預覽
    $(".preview_fancybox").fancybox();

    $.LoadingOverlaySetup(set_loading_bar);
    toastr_success.onHidden = function() {};
    toastr_error.onHidden = function() {};
    //列表switch效果


    $('.item_status').bootstrapSwitch({
        size:'mini',
        onColor: 'success',
        offColor: 'default',
        onText: '啟用',
        offText: '關閉',
        onSwitchChange: function (event, state) {
            console.log(state + ' - 編號：' + $(this).attr('uuid'));
            $.LoadingOverlay("show");
            action = 'status';
            _uuid = $(this).attr('uuid');
            do_submit(action,_uuid)
            return;
        }
    });
    $('.add_btn').click(function(){
        location.href = base_url + cms_path +  'banner_content';
    });

    $('.delete_btn').click(function(){
        tmp_btn = $(this);
        action = 'delete';
        _uuid = $(this).attr('uuid');
        bootbox.confirm({
            message: "確定要刪除?",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> 取消',
                    className: 'btn-sm',
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> 確定',
                    className: 'btn-sm btn-danger',
                }
            },
            callback: function (result) {

                if(result) {
                    do_submit(action,_uuid)
                }

            }
        });

    });

    $('.modify_btn').click(function(){
        location.href = base_url + cms_path +  'banner_content/modify/' + $(this).attr('uuid');

    });

    $('.view_btn').click(function(){
        location.href = base_url + cms_path +  'banner_content/view/' + $(this).attr('uuid');

    });

    $('.search_btn').click(function(){

        console.log('search_btn');
        $('#frm_search').attr('action',  base_url + cms_path +  'banners');
        $('#frm_search').submit();

    });

    $('.reset_btn').click(function(){
        location.href = base_url + cms_path +  'banners/reset';
        console.log('reset_btn');

    });

    //修改排序

    $('.order_num').editable({
        type : 'number',
        url:  base_url + cms_path + 'banners/change_order',
        title: 'Enter Order number',
        params: function(params) {
            params.uuid = $(this).attr('uuid');
            params[$('#csrf').attr('name')] = $('#csrf').attr('value');
            return params;
        },
        success: function(response, newValue) {
            if(!response) {
                return "Unknown error!";
            }
            console.log(newValue);
            if(response.success === false) {
                toastr.success('', '排序更改完成' , toastr_success);
                return response.msg;
            }
        },
        display: function(value) {
            $(this).text(parseInt(value));
        }
    });

    //排序操作
    $('.do_sort').click(function(){
        page_name = getBaseName(window.location.href);
        if($(this).attr('sort') == '' || $(this).attr('sort') == 'ASC'){
            $(this).attr('sort','DESC');
        }else{
            $(this).attr('sort','ASC');
        }

        action = 'sort';
        frm_01_FormData = new FormData(frm_01[0]);
        frm_01_FormData.append('do', action);
        frm_01_FormData.append('uuid',  $(this).attr('uuid'));
        frm_01_FormData.append('filed', $(this).attr('id'));
        frm_01_FormData.append('sort', $(this).attr('sort'));
        frm_01_FormData.append('ss_sort_name', page_name);
        axios({
            method: 'post',
            baseURL: send_change_order_url ,
            headers: {'Content-Type': 'multipart/form-data' },
            data: frm_01_FormData
        }).then(function (response) {
            //handle success
            $.LoadingOverlay("hide");
            if(response.data.result == 'success'){
                toastr.success('', '重新排序完成' , toastr_success);
                location.reload();
            }else{
                toastr.error('排序有誤', '' , toastr_error);
            }

        }).catch(function (response) {
            toastr.error('排序有誤', '' , toastr_error);
        });
    });

    $('.do_sort').attr('sort',ss_sort);

});

function do_submit(action,uuid){
    frm_01_FormData = new FormData(frm_01[0]);
    frm_01_FormData.append('do', action);
    frm_01_FormData.append('uuid', uuid);
    axios({
        method: 'post',
        baseURL: send_status_url ,
        headers: {'Content-Type': 'multipart/form-data' },
        data: frm_01_FormData
    }).then(function (response) {
        //handle success
        $.LoadingOverlay("hide");
        if(response.data.result == 'success'){
            toastr.success('', '狀態已更改完成' , toastr_success);
            if(action == 'delete'){
                $('.row_' + uuid) .css({
                    'text-decoration':'line-through',
                });
                $('.row_' + uuid + ' .control_group') .html('此筆資料已刪除');
            }
        }else{
            toastr.error('資料有誤', '' , toastr_error);
            $('#send_form_btn').removeAttr('disabled');
        }

    }).catch(function (response) {
        toastr.error('資料有誤', '' , toastr_error);
        $('#send_form_btn').removeAttr('disabled');
    });
}

