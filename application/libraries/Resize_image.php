<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * UUID Class
 *
 * This implements the abilities to create UUID's for CodeIgniter.
 * Code has been borrowed from the followinf comments on php.net
 * and has been optimized for CodeIgniter use.
 * http://www.php.net/manual/en/function.uniqid.php#94959
 *
 * @category Libraries
 * @author Dan Storm
 * @link http://catalystcode.net/
 * @license GNU LPGL
 * @version 2.1 
 */

class Resize_image{

    public $type;//圖片類型

    public $width;//實際寬度

    public $height;//實際高度

    public $resize_width;//改變後的寬度

    public $resize_height;//改變後的高度

    public $cut;//是否裁圖

    public $srcimg;//源圖象

    public $dstimg;//目標圖象地址

    public $im;//臨時創建的圖象

    public $quality;//圖片質量

    public $img_array=array('jpg','png','gif');

    //http://www.phpernote.com/php-function/782.html

    function compress($img,$wid,$hei,$c,$dstpath,$quality=100){

        $this->srcimg=$img;

        $this->resize_width=$wid;

        $this->resize_height=$hei;

        $this->cut=$c;

        $this->quality=$quality;

        //$this->type=strtolower(substr(strrchr($this->srcimg,'.'),1));//圖片的類型

        $this->type=$this->checkFileType($this->srcimg);//更為嚴格的檢測圖片類型

        if(!in_array($this->type,$this->img_array)){

            return '';

        }

        $this->initi_img();//初始化圖象

        $this -> dst_img($dstpath);//目標圖象地址

        $this->width=imagesx($this->im);

        $this->height=imagesy($this->im);

        $this->newimg();//生成圖象

        ImageDestroy($this->im);

    }

    function newimg(){

        $resize_ratio=($this->resize_width)/($this->resize_height);//改變後的圖象的比例

        $ratio=($this->width)/($this->height);//實際圖象的比例

        if(($this->cut)=='1'){//裁圖

            if(function_exists('imagepng')&&(str_replace('.','',PHP_VERSION)>=512)){//針對php版本大於5.12參數變化後的處理情況

                $quality=9;

            }

            if($ratio>=$resize_ratio){//高度優先

                $newimg=imagecreatetruecolor($this->resize_width,$this->resize_height);

                //上色
                $color=imagecolorallocate($newimg,255,255,255);
                //設置透明
                imagecolortransparent($newimg,$color);
                imagefill($newimg,0,0,$color);

                imagecopyresampled($newimg,$this->im,0,0,0,0,$this->resize_width,$this->resize_height,(($this->height)*$resize_ratio),$this->height);

                imagejpeg($newimg,$this->dstimg,$this->quality);

            }

            if($ratio<$resize_ratio){//寬度優先

                $newimg=imagecreatetruecolor($this->resize_width,$this->resize_height);

                //上色
                $color=imagecolorallocate($newimg,255,255,255);
                //設置透明
                imagecolortransparent($newimg,$color);
                imagefill($newimg,0,0,$color);

                imagecopyresampled($newimg,$this->im,0,0,0,0,$this->resize_width,$this->resize_height,$this->width,(($this->width)/$resize_ratio));

                imagejpeg($newimg,$this->dstimg,$this->quality);

            }

        }else{//不裁圖

            if($ratio>=$resize_ratio){

                $newimg=imagecreatetruecolor($this->resize_width,($this->resize_width)/$ratio);

                //上色
                $color=imagecolorallocate($newimg,255,255,255);
                //設置透明
                imagecolortransparent($newimg,$color);
                imagefill($newimg,0,0,$color);

                imagecopyresampled($newimg,$this->im,0,0,0,0,$this->resize_width,($this->resize_width)/$ratio,$this->width,$this->height);

                imagejpeg($newimg,$this->dstimg,$this->quality);

            }

            if($ratio<$resize_ratio){

                $newimg=imagecreatetruecolor(($this->resize_height)*$ratio,$this->resize_height);

                //上色
                $color=imagecolorallocate($newimg,255,255,255);
                //設置透明
                imagecolortransparent($newimg,$color);
                imagefill($newimg,0,0,$color);

                imagecopyresampled($newimg,$this->im,0,0,0,0,($this->resize_height)*$ratio,$this->resize_height,$this->width,$this->height);

                imagejpeg($newimg,$this->dstimg,$this->quality);

            }

        }

    }

    function initi_img(){//初始化圖象

        if($this->type=='jpg'){

            $this->im=imagecreatefromjpeg($this->srcimg);

        }

        if($this->type=='gif'){

            $this->im=imagecreatefromgif($this->srcimg);

        }

        if($this->type=='png'){

            $this->im=imagecreatefrompng($this->srcimg);

        }

    }

    function dst_img($dstpath){//圖象目標地址

        $full_length=strlen($this->srcimg);

        $type_length=strlen($this->type);

        $name_length=$full_length-$type_length;

        $name=substr($this->srcimg,0,$name_length-1);

        $this->dstimg=$dstpath;

        //echo $this->dstimg;

    }

    //讀取文件前幾個字節 判斷文件類型

    function checkFileType($filename){

        $file=fopen($filename,'rb');

        $bin=fread($file,2); //只讀2字節

        fclose($file);

        $strInfo =@unpack("c2chars",$bin);

        $typeCode=intval($strInfo['chars1'].$strInfo['chars2']);

        $fileType='';

        switch($typeCode){

            case 7790:

                $fileType='exe';

                break;

            case 7784:

                $fileType='midi';

                break;

            case 8297:

                $fileType='rar';

                break;

            case 255216:

                $fileType='jpg';

                break;

            case 7173:

                $fileType='gif';

                break;

            case 6677:

                $fileType='bmp';

                break;

            case 13780:

                $fileType='png';

                break;

            default:

                $fileType='unknown'.$typeCode;

                break;

        }

        if($strInfo['chars1']=='-1'&&$strInfo['chars2']=='-40'){

            return 'jpg';

        }

        if($strInfo['chars1']=='-119'&&$strInfo['chars2']=='80'){

            return 'png';

        }

        return $fileType;

    }

}