<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Crypt
{

        public function encryptdata($data) {
        $ci = &get_instance();
        $ENCRYPT_DEFAULT_KEY = $ci->config->item('encryptionKey');
        $ENCRYPT_DEFAULT_IV = $ci->config->item('encryptionIV');
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        $cipher = "aes-128-cbc";
        $encryptstr = openssl_encrypt($data, $cipher, $ENCRYPT_DEFAULT_KEY, $options=0, $ENCRYPT_DEFAULT_IV);
        return base64_encode($encryptstr);
    }

        public function decryptdata($data) {
        $ci = &get_instance();
        $ENCRYPT_DEFAULT_KEY = $ci->config->item('encryptionKey');
        $ENCRYPT_DEFAULT_IV = $ci->config->item('encryptionIV');
        $data = base64_decode($data);
        $cipher = "aes-128-cbc";
        $decryptstr = openssl_decrypt($data, $cipher, $ENCRYPT_DEFAULT_KEY, $options=0, $ENCRYPT_DEFAULT_IV);
        $padding = ord($decryptstr[strlen($decryptstr) - 1]);
        $decryptstr = substr($decryptstr, 0, -$padding);
        return $decryptstr;
    }
}