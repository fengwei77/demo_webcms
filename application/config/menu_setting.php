<?php
/**
 * Created by PhpStorm.
 * User: fengw
 * Date: 2018/9/20
 * Time: 下午 04:12
 */
$config['side_menu'] =array(
    0 =>  array(
        'id' => 'm0',
        'name' => '無',
        'front_menu_name' => '無',
        'tag' => 'none',
        'pagename' => '',
        'status' => '0',
    ),
    1 =>  array(
        'id' => 'm1',
        'name' => '權限',
        'front_menu_name' => '無',
        'tag' => 'm-a',
        'pagename' =>'',
        'status' => '1',
        'sub_menu' => array(
            0 => array( 'id' => 'm10', 'name' => '群組管理' , 'pagename' =>'user_groups','status' => 1) ,
            1 => array( 'id' => 'm11','name' => '帳號管理' , 'pagename' =>'users','status' => 1) ,
        ),
    ),
    2 =>  array(
        'id' => 'm2',
        'name' => '首頁輪撥',
        'front_menu_name' => '無',
        'tag' => 'm-b',
        'pagename' =>'',
        'status' => '1',
        'sub_menu' => array(
            0 => array( 'id' => 'm20', 'name' => '圖片管理' , 'pagename' =>'banners','status' => 1) ,
        ),
    ),
    3 =>  array(
        'id' => 'm3',
        'name' => '活動資訊',
        'front_menu_name' => '無',
        'tag' => 'm-e',
        'pagename' =>'',
        'status' => '1',
        'sub_menu' => array(
            0 => array( 'id' => 'm30', 'name' => '內容管理' , 'pagename' => 'events','status' => 1) ,
        ),
    ),
//    4 =>  array(
//        'id' => 'm4',
//        'name' => '臉書分享資料',
//        'front_menu_name' => '無',
//        'tag' => 'm-f',
//        'pagename' =>'',
//        'status' => '1',
//        'sub_menu' => array(
//            0 => array( 'id' => 'm40', 'name' => '資料管理' , 'pagename' => 'fb','status' => 1) ,
//        ),
//    ),
);