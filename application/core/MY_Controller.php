<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class MY_Controller extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function load_cms_template($opt_js = null , $opt_css  = null , $opt_template = "cms_template" , $ext_data = null){

        // **PREVENTING SESSION HIJACKING**
        ini_set('session.cookie_httponly', 1);
        ini_set('session.use_only_cookies', 1);
        // Uses a secure connection (HTTPS) if possible
        ini_set('session.cookie_secure', 1);
        header("X-Frame-Options: sameorigin");
        header("X-XSS-Protection: 1; mode=block");
//        header("Content-Security-Policy: default-src 'self' *.bootstrapcdn.com *.googleapis.com *.googletagmanager.com  *.gstatic.com; script-src 'self'  *.facebook.net  *.bootstrapcdn.com *.googleapis.com *.googletagmanager.com; connect-src 'self' *.bootstrapcdn.com *.googleapis.com *.googletagmanager.com; img-src 'self' *.bootstrapcdn.com *.googleapis.com *.googletagmanager.com; style-src 'self' *.bootstrapcdn.com *.googleapis.com *.googletagmanager.com;");


        //記選單位置
//        $_SESSION['session_cms_menu'] = '';
        if(!isset($_SESSION['session_cms_menu'])){
            $_SESSION['session_cms_menu'] = '';
        }
        if(!isset( $_SESSION['ss_pagename'])){
            $_SESSION['ss_pagename'] = '';
        }
        //判斷登入狀態
        if(!isset($_SESSION['manager_data']['status']) || $_SESSION['manager_data']['status'] !== 'passed'){

            header("Refresh: 0; url=". base_url($ext_data["webcms_controllers_folder"].'login'));
            die();
        }

        $data = null;

        if(!is_null($opt_js)){
            $data['opt_js'] = $opt_js;
        }
        if(!is_null($opt_css)){
            $data['opt_css'] = $opt_css;
        }
        if(is_null($opt_template)){
            $data['opt_template'] = "cms_template";
        }



        //加入要輸出的資料
        if(!is_null($ext_data)){
            $ext_data['mng_data'] = array(
                "name" =>  $_SESSION['manager_data']['name'],
                "status" =>  $_SESSION['manager_data']['status'],
            );
            $data['ext_data'] = $ext_data;
        }



        $this->load->view('webcms/'.$data['opt_template'] ,$data);

    }



}