<?php
/**
 * Created by PhpStorm.
 * User: fengw
 * Date: 2018/10/2
 * Time: 上午 10:19
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_content extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('basic_model');  //選擇MODEL
        $this->load->model('event_model');  //選擇MODEL
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');

    }

    public function index_get($page = 1)
    {
        $_SESSION['session_cms_menu'] = 'e';
        if(  $_SESSION['ss_pagename'] != $this->router->fetch_class()){
            unset($_SESSION['ss_sort_name']);
            unset($_SESSION['ss_sort_filed']);
            unset($_SESSION['ss_sort']);
        }
        $_SESSION['ss_pagename'] = $this->router->fetch_class();
        if(!isset( $_SESSION[$this->router->fetch_class() . '-timer'])){
            $_SESSION[$this->router->fetch_class() . '-timer'] = time();
        }else{
            $perv_time =  $_SESSION[$this->router->fetch_class() . '-timer'];
            if( (time() - $perv_time) > $this->config->item('list_fresh_time') ){
                $_SESSION[$this->router->fetch_class() . '-timer'] = time();
                header("Refresh:0");
                die();
            }
        }
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '新增內容',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'search_date' => 0,
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );

        //套件
         $custom_css = array(
            'cropper' => base_url('bower_components/cropper/cropper.min.css'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.css'),
            'bootstrap-datetimepicker' => base_url('bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'),
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
            'switch_box' => base_url('assets/webcms/css/switch_box.css'),
        );

        $custom_js =array(
            'tinymce' => base_url('bower_components/tinymce/tinymce.min.js'),
            'moment.js' => base_url('bower_components/moment.js/moment.js'),
            'moment.js-locales' => base_url('bower_components/moment.js/moment-with-locales.js'),
            'bootstrap-datetimepicker' => base_url('bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js'),
            'bootstrap-filestyle' => base_url('bower_components/bootstrap-filestyle/bootstrap-filestyle.min.js'),
            'cropper' => base_url('bower_components/cropper/cropper.min.js'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.js'),
            'image_cropper' => base_url('assets/webcms/js/image_upload_with_cropper.js'),
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/event_content.js?t='.time()),
            'pagination' => base_url('assets/webcms/js/pagination.js'),

        );



        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }

        $ext_data['uuid']  = '';

        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/event_content/vw_index',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }

    /*新增資料*/
    /**
     * @param null $gid
     */

    public function add_item_post()
    {
        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
            die();
        }
        //接收資料

        $title = $this->input->post('title', TRUE);
        $ref_id = "";
        $short_title = $this->input->post('short_title', TRUE);
        $desc = "";
        $source = $this->input->post('source', TRUE);
        $quick_read = $this->input->post('quick_read', TRUE);
        $section_open = "1,1,1";
        $title_1 = is_null( $this->input->post('title_1', TRUE)) ? '' :$this->input->post('title_1', TRUE);
        $content_1 = is_null( $this->input->post('content_1', TRUE)) ? '' :$this->input->post('content_1', TRUE);
        $point_1 = is_null( $this->input->post('point_1', TRUE)) ? '' :$this->input->post('point_1', TRUE);
        $title_2 = is_null( $this->input->post('title_2', TRUE)) ? '' :$this->input->post('title_2', TRUE);
        $content_2 = is_null( $this->input->post('content_2', TRUE)) ? '' :$this->input->post('content_2', TRUE);
        $point_2 = is_null( $this->input->post('point_2', TRUE)) ? '' :$this->input->post('point_2', TRUE);
        $title_3 = is_null( $this->input->post('title_3', TRUE)) ? '' :$this->input->post('title_3', TRUE);
        $content_3 = is_null( $this->input->post('content_3', TRUE)) ? '' :$this->input->post('content_3', TRUE);
        $point_3 = is_null( $this->input->post('point_3', TRUE)) ? '' :$this->input->post('point_3', TRUE);
        $template_type = '';
        $item_date = date("Y-m-d H:i:s");
        $start_date = $this->input->post('start_date', TRUE). ' 00:00:00' ;
        $expire_date = $this->input->post('expire_date', TRUE). ' 23:59:59' ;
        $tags = "";
        $keywords = "";
        $files = $this->input->post('files', TRUE);
        $order_num = is_null($this->input->post('order_num', TRUE)) ?  0 :  $this->input->post('order_num', TRUE);
        $item_status = is_null($this->input->post('item_status', TRUE)) ?  0 :  $this->input->post('item_status', TRUE);

        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->output->set_content_type("application/json"); //輸出格式
        $result_output = array("error_code" => "" ,"action" => "" ,"result" => ""); //結果

        //檔案位置分配
        $file_1 = $this->input->post('file_1', TRUE) != null ? $this->input->post('file_1', TRUE) : '';
        $file_2 = $this->input->post('file_2', TRUE) != null ? $this->input->post('file_2', TRUE) : '';


        $new_uuid = $this->uuid->v1();

        $request_data = array(
            "uuid" => $new_uuid ,
            "ref_id" => $ref_id ,
            "title" => $title ,
            "short_title" => $short_title ,
            "list_pic" => $file_1 ,
            "desc" => htmlentities($desc, ENT_QUOTES, "UTF-8") ,
            "source" =>  htmlentities($source, ENT_QUOTES, "UTF-8") ,
            "quick_read" =>   htmlentities($quick_read, ENT_QUOTES, "UTF-8") ,
            "section_open" => $section_open ,
            "title_1" => $title_1 ,
            "content_1" =>  htmlentities($content_1, ENT_QUOTES, "UTF-8") ,
            "point_1" =>    htmlentities($point_1, ENT_QUOTES, "UTF-8") ,
            "title_2" => $title_2 ,
            "content_2" =>  htmlentities($content_2, ENT_QUOTES, "UTF-8") ,
            "point_2" =>  htmlentities($point_2, ENT_QUOTES, "UTF-8") ,
            "title_3" => $title_3 ,
            "content_3" =>  htmlentities($content_3, ENT_QUOTES, "UTF-8") ,
            "point_3" =>  htmlentities($point_3, ENT_QUOTES, "UTF-8") ,
            "template_type" => $template_type ,
            "item_date" => $item_date ,
            "start_date" => $start_date ,
            "expire_date" => $expire_date ,
            "tags" => $tags ,
            "keywords" => $keywords ,
            "files" =>  ( $files ) ,
            "hot" => 0 ,
            "sort_number" => $order_num ,
            "verify" => 0 ,
            "item_status" => $item_status ,
            "has_deleted" => 0 ,
            "create_user_uuid" => $_SESSION['manager_data']['uuid']  ,
            "modify_user_uuid" => '' ,
            "update_datetime" =>  $data['update_datetime']
        );

        $this->basic_model->initialize( "tb_articles" );
        $insert_id = $this->basic_model->insert($request_data);

        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "活動",
            "",
            "新增",
            "內容"
        );
        if($insert_id > 0){
            $result_output = array("error_code" => "0" ,"action" => "add" ,"result" => "success" , "id" => $insert_id); //結果
        }else{
            $result_output = array("error_code" => "1" ,"action" => "add" ,"result" => "fail" , "id" => '' ,"message" => '資料存入失敗'); //結果
        }

        echo json_encode($result_output);

        return true;
    }

    /*修改資料*/
    /**
     * @param null $uuid
     */
    public function modify_get($uuid = null)
    {

        $_SESSION['session_cms_menu'] = 'e';
        if(  $_SESSION['ss_pagename'] != $this->router->fetch_class()){
            unset($_SESSION['ss_sort_name']);
            unset($_SESSION['ss_sort_filed']);
            unset($_SESSION['ss_sort']);
        }
        $_SESSION['ss_pagename'] = $this->router->fetch_class();
        if(!isset( $_SESSION[$this->router->fetch_class() . '-timer'])){
            $_SESSION[$this->router->fetch_class() . '-timer'] = time();
        }else{
            $perv_time =  $_SESSION[$this->router->fetch_class() . '-timer'];
            if( (time() - $perv_time) > $this->config->item('list_fresh_time') ){
                $_SESSION[$this->router->fetch_class() . '-timer'] = time();
                header("Refresh:0");
                die();
            }
        }
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '修改內容',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'search_date' => 0,
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );

        //套件
        $custom_css = array(
            'cropper' => base_url('bower_components/cropper/cropper.min.css'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.css'),
            'bootstrap-datetimepicker' => base_url('bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'),
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
            'switch_box' => base_url('assets/webcms/css/switch_box.css'),
        );

        $custom_js =array(
            'tinymce' => base_url('bower_components/tinymce/tinymce.min.js'),
            'moment.js' => base_url('bower_components/moment.js/moment.js'),
            'moment.js-locales' => base_url('bower_components/moment.js/moment-with-locales.js'),
            'bootstrap-datetimepicker' => base_url('bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js'),
            'bootstrap-filestyle' => base_url('bower_components/bootstrap-filestyle/bootstrap-filestyle.min.js'),
            'cropper' => base_url('bower_components/cropper/cropper.min.js'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.js'),
            'image_cropper' => base_url('assets/webcms/js/image_upload_with_cropper.js'),
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/event_content.js?t='.time()),
            'pagination' => base_url('assets/webcms/js/pagination.js'),

        );



        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }

        $ext_data['uuid']  = '';

        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //取得 item $uuid

        if(isset($uuid) ){
            $ext_data['uuid'] =  $uuid;
            $this->basic_model->initialize( "tb_articles" );
             $where_arr = array(
                'uuid' => $uuid,
                'has_deleted' => 0
            );
            $ext_data['output_data'] = array();
            $ext_data['query_result'] = $this->basic_model->get_where($where_arr);
            foreach($ext_data['query_result'] as $row){
//                //取資料
                $ext_data['output_data']  = array(
                    'id' => $row->id,
                    'uuid' => $row->uuid,
                    "ref_id" => $row->ref_id,
                    "title" => $row->title,
                    "short_title" => $row->short_title,
                    "list_pic" => $row->list_pic,
                    "desc" => $row->desc,
                    "source" => $row->source ,
                    "quick_read" =>    $row->quick_read ,
                    "section_open" => $row->section_open ,
                    "title_1" => $row->title_1 ,
                    "content_1" =>  $row->content_1,
                    "point_1" =>    $row->point_1,
                    "title_2" => $row->title_2 ,
                    "content_2" =>  $row->content_2,
                    "point_2" =>  $row->point_2,
                    "title_3" => $row->title_3 ,
                    "content_3" =>  $row->content_3,
                    "point_3" =>  $row->point_3,
                    "template_type" => $row->template_type ,
                    "item_date" =>  $row->item_date ,
                    "start_date" =>  $row->start_date ,
                    "expire_date" =>  $row->expire_date ,
                    "tags" =>  $row->tags ,
                    "keywords" =>  $row->keywords ,
                    "files" =>  $row->files ,
                    "hot" => 0 ,
                    "sort_number" =>  $row->sort_number ,
                    "verify" => 0 ,
                    "item_status" => $row->item_status ,
                    "has_deleted" => 0 ,
                    "create_user_uuid" => $row->create_user_uuid,
                    "modify_user_uuid" =>  $row->modify_user_uuid,
                    'update_datetime' => $row->update_datetime,
                    'create_datetime' => $row->create_datetime
                );


                //使用者記錄
                $this->basic_model->initialize( "tb_users" );
                $where_arr = array(
                    'uuid' => $row->modify_user_uuid
                );
                $ext_data['query_result_update_data'] = $this->basic_model->get_where($where_arr);

                $where_arr = array(
                    'uuid' => $row->create_user_uuid
                );
                $ext_data['query_result_create_data'] = $this->basic_model->get_where($where_arr);


            }

        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/events'), 'location', 301);
        }


        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/event_content/vw_modify',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }


    /*更新資料*/
    /**
     * @param null $gid
     */
    public function update_item_post()
    {
        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
            die();
        }
        //接收資料
        $uuid = $this->input->post('uuid', TRUE);

        $title = $this->input->post('title', TRUE);
        $ref_id = "";
        $short_title = $this->input->post('short_title', TRUE);
        $desc = "";
        $source = $this->input->post('source', TRUE);
        $quick_read = $this->input->post('quick_read', TRUE);
        $section_open = "1,1,1";
        $title_1 = is_null( $this->input->post('title_1', TRUE)) ? '' :$this->input->post('title_1', TRUE);
        $content_1 = is_null( $this->input->post('content_1', TRUE)) ? '' :$this->input->post('content_1', TRUE);
        $point_1 = is_null( $this->input->post('point_1', TRUE)) ? '' :$this->input->post('point_1', TRUE);
        $title_2 = is_null( $this->input->post('title_2', TRUE)) ? '' :$this->input->post('title_2', TRUE);
        $content_2 = is_null( $this->input->post('content_2', TRUE)) ? '' :$this->input->post('content_2', TRUE);
        $point_2 = is_null( $this->input->post('point_2', TRUE)) ? '' :$this->input->post('point_2', TRUE);
        $title_3 = is_null( $this->input->post('title_3', TRUE)) ? '' :$this->input->post('title_3', TRUE);
        $content_3 = is_null( $this->input->post('content_3', TRUE)) ? '' :$this->input->post('content_3', TRUE);
        $point_3 = is_null( $this->input->post('point_3', TRUE)) ? '' :$this->input->post('point_3', TRUE);
        $template_type = '';
        $item_date = date("Y-m-d H:i:s");
        $start_date = $this->input->post('start_date', TRUE). ' 00:00:00' ;
        $expire_date = $this->input->post('expire_date', TRUE). ' 23:59:59' ;
        $tags = "";
        $keywords = "";
        $files = $this->input->post('files', TRUE);
        $order_num = is_null($this->input->post('order_num', TRUE)) ?  0 :  $this->input->post('order_num', TRUE);
        $item_status = is_null($this->input->post('item_status', TRUE)) ?  0 :  $this->input->post('item_status', TRUE);

        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->output->set_content_type("application/json"); //輸出格式
        $result_output = array("error_code" => "" ,"action" => "" ,"result" => ""); //結果

        //檔案位置分配
        $file_1 = $this->input->post('file_1', TRUE) != null ? $this->input->post('file_1', TRUE) : '';
        $file_2 = $this->input->post('file_2', TRUE) != null ? $this->input->post('file_2', TRUE) : '';


        $where_arr =  array( "uuid" => $uuid );
        $request_data = array(
            "ref_id" => $ref_id ,
            "title" => $title ,
            "short_title" => $short_title ,
            "list_pic" => $file_1 ,
            "desc" => htmlentities($desc, ENT_QUOTES, "UTF-8") ,
            "source" =>  htmlentities($source, ENT_QUOTES, "UTF-8") ,
            "quick_read" =>   htmlentities($quick_read, ENT_QUOTES, "UTF-8") ,
            "section_open" => $section_open ,
            "title_1" => $title_1 ,
            "content_1" =>  htmlentities($content_1, ENT_QUOTES, "UTF-8") ,
            "point_1" =>    htmlentities($point_1, ENT_QUOTES, "UTF-8") ,
            "title_2" => $title_2 ,
            "content_2" =>  htmlentities($content_2, ENT_QUOTES, "UTF-8") ,
            "point_2" =>  htmlentities($point_2, ENT_QUOTES, "UTF-8") ,
            "title_3" => $title_3 ,
            "content_3" =>  htmlentities($content_3, ENT_QUOTES, "UTF-8") ,
            "point_3" =>  htmlentities($point_3, ENT_QUOTES, "UTF-8") ,
            "template_type" => $template_type ,
            "item_date" => $item_date ,
            "start_date" => $start_date ,
            "expire_date" => $expire_date ,
            "tags" => $tags ,
            "keywords" => $keywords ,
            "files" =>  ( $files ) ,
            "hot" => 0 ,
            "sort_number" => $order_num ,
            "verify" => 0 ,
            "item_status" => $item_status ,
            "has_deleted" => 0 ,
            "create_user_uuid" => $_SESSION['manager_data']['uuid']  ,
            "modify_user_uuid" => '' ,
            "update_datetime" =>  $data['update_datetime']
        );

        $this->basic_model->initialize( "tb_articles" );
        $update_id = $this->basic_model->update($request_data,$where_arr);


        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "活動內容",
            $update_id,
            "修改",
            "內容"
        );
        $result_output = array("error_code" => "0" ,"action" => "update" ,"result" => "success" ); //結果

        echo json_encode($result_output);

        return true;
    }




}
