<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ajaxupload
 *
 * @author http://roytuts.com
 */
class Ajax_upload extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('cookie');
        $this->load->helper('url');
    }


    function index()
    {
        $data = null; // init
        $now_time = date("Y-m-d");
        $webcms_prename = $this->config->item('webcms_prename');

        $data = array(
            'webcms_title' => $this->config->item('webcms_title'),
            'webcms_style' => $this->config->item('webcms_style'),
            'assets' => $this->config->item('assets'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'manager_name' => $this->session->userdata('manager_name')
        );

        $_SESSION['tinymce_base_sub_url'] = $this->config->item('tinymce_base_sub_url');  // 紀錄tinymce子資料匣名稱可為空

        $this->load->view('webcms/file_upload_ajax', $data);
    }

    function upload_file()
    {
        $this->output->set_content_type('application/json'); // 輸出格式
        $output = array(
            'action' => "add",
            'result' => "failed",
            'file' => ""
        ); // 結果

        // upload file config
        $config['quality'] = '100%';

        $folder_date_name = date('Y').'/'. date('m');
        $dir = $this->config->item('upload_folder') . $folder_date_name . '/';

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);  // create directory if not exist

        }

        $config['upload_path'] = $dir;
        $config['allowed_types'] = '*';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = FALSE;
        $config['overwrite'] = FALSE;
        $config['max_size'] = 1024 * 10; //1 MB
        $config['file_name'] = $this->uuid->v1();
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

        if (!$this->upload->do_upload('files')) {
            $output = array(
                'action' => "add",
                'result' => "failed",
                'file' => '',
                "error" => $this->upload->display_errors()
            ); // 結果
        }
        else {
            $file_origin_name = $this->upload->data('file_name');
            $uuid = $this->uuid->v1();
             if(  $this->upload->data('image_width]')  > 800  ||   $this->upload->data('image_height]')  > 800)
             {
                 $width = 300;
                 $height = 300;
             }else{
                 $width = $this->upload->data('image_width]');
                 $height =  $this->upload->data('image_height]');
             }
            $thumbs_dir = $config['upload_path'] . 'thumbs/';

            if (!file_exists($thumbs_dir)) {
                mkdir($thumbs_dir, 0777, true);  // create directory if not exist
            }

            $image_origin = $dir . $file_origin_name;
            $image_thumb = $thumbs_dir . $uuid . $this->upload->data('file_ext');
            $data = getimagesize($image_origin);
            $width = $data[0];
            $height = $data[1];
            $this->load->library('resize_image');
            $this->resize_image->compress($image_origin,$width,$height,0,$image_thumb,95);

            $output = array(
                    'action' => "add",
                    'result' => "success",
                    'file' => $image_thumb,
                    'uuid' => $uuid
                ); // 結果
        }

        echo json_encode($output);

    }


    function cropper()
    {
        // upload file config
        $output = array(
            'action' => "crop",
            'result' => "failed",
            'file' => ""
        ); // 結果
        $config['quality'] = '100%';
        $folder_date_name = date('Y').'/'. date('m');
        $dir = $this->config->item('upload_folder') . 'crop/' . $folder_date_name . '/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);  // create directory if not exist
        }
        $config['upload_path'] = $dir;
        $config['allowed_types'] = '*';
        // $config['allowed_types'] = 'jpg|png';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = FALSE;
        $config['overwrite'] = FALSE;
        $config['max_size'] = 1024 * 5; //1 MB
        $config['file_name'] = $this->uuid->v1() . '-origin';
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

        if (!$this->upload->do_upload('croppedImage')) {
            $error = array('error' => $this->upload->display_errors());
            echo $this->upload->display_errors();
        } else {
            $file_origin_name = $this->upload->data('file_name');
            $uuid = $this->uuid->v1();
            $image_origin = $config['upload_path'] . $file_origin_name;
            $image_thumb = $config['upload_path'] . $uuid . '.png';
            $data = getimagesize($image_origin);
            $width = $data[0];
            $height = $data[1];
            $this->load->library('resize_image');
            $this->resize_image->compress($image_origin,$width,$height,0,$image_thumb,90);

            unlink($image_origin);
            $output = array(
                'action' => "add",
                'result' => "success",
                'file' => $dir . $uuid . '.png'
            ); // 結果

        }
        echo json_encode($output);

    }

    function upload_from_url()
    {
        $output = array(
            'action' => "save_file",
            'result' => "failed",
            'file' => ""
        ); // 結果

        $url = $this->input->post('url');
        $folder_date_name = date('Y').'/'. date('m');
        $dir = $this->config->item('upload_folder') . 'community/' . $folder_date_name . '/';

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);  //create directory if not exist
        }

        $path_parts = pathinfo($url);

        $file_exif = (empty($path_parts['extension'])) ? '' : strtolower(
            substr($path_parts['extension'], 0, strrpos($path_parts['extension'], '?'))
        );

        if ($file_exif == '') {
            $file_exif = 'jpg';
        }
        // $url = 'http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/558792_620258731339858_1638250579_n.jpg';
        $gid = $this->uuid->v1();
        $filename = $dir . $gid . '.' . $file_exif;

        $timeout = 50;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $img = curl_exec($ch);
        curl_close($ch);

        $size = strlen($img);

        //文件大小
        $fp2 = @fopen($filename, 'w');

        fwrite($fp2, $img);
        fclose($fp2);
        $output = array(
            'action' => "save_file",
            'result' => "success",
            'file' => $filename
        ); // 結果

        echo json_encode($output);
    }

}
