<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_size extends MY_Controller {

	public function index_post()
	{
        $ss_page_size_name = $this->input->post('page_size_name');
        $page_size = $this->input->post('page_size');

        $_SESSION['ss_page_size_name'] = $ss_page_size_name;
        $_SESSION['ss_page_size'] = $page_size ;

        if(is_null($ss_page_size_name)){
            $result_output = array("error_code" => "1" ,"action" => "add" ,"result" => "fail" , "id" => ''); //結果
            echo json_encode($result_output);
            return true;
        }else{
            $result_output = array("error_code" => "0" ,"action" => "add" ,"result" => "success" , "id" => ''); //結果
            echo json_encode($result_output);
            return true;
        }

	}

	public function sort_func_post(){
        $do = $this->input->post('do');
        $filed = $this->input->post('filed');
        $sort = $this->input->post('sort');
        $ss_sort_name = $this->input->post('ss_sort_name');
        $_SESSION['ss_sort_name'] = $ss_sort_name;
        $_SESSION['ss_sort'] = $sort ;
        $_SESSION['ss_sort_filed'] = $filed ;
        $result_output = array("error_code" => "0" ,"action" => "add" ,"result" => "success" , "id" => ''); //結果
        echo json_encode($result_output);
        return true;
    }
}
