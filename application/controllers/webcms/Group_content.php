<?php
/**
 * Created by PhpStorm.
 * User: fengw
 * Date: 2018/9/21
 * Time: 下午 02:58
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_content extends MY_Controller {
    protected  $unit_title = "群組管理";
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('basic_model');  //選擇MODEL
        $this->load->helper('cookie');
        $this->load->helper('url');

    }
    public function index_get()
    {

        $_SESSION['session_cms_menu'] = 'a';
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => $this->unit_title,
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );
        //套件
        $custom_css = array(
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
        );

        $custom_js =array(
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/group_content.js'),
        );

        $ext_data['uuid']  = '';

        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
//            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }

        //
        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/group_content/vw_index',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }


    /*新增資料*/

    /**
     * @param null $gid
     */

    public function add_item_post()
    {
        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            $result_output = array("error_code" => "0" ,"action" => "" ,"result" => "login" ); //結果
            echo json_encode($result_output);
            die();
        }
        //接收資料

        $name = $this->input->post('name', TRUE);
        $name = str_replace("系統管理","_系統管理" , $name);
        $roles_arr = $this->input->post('roles_arr', TRUE);
        $item_status = is_null($this->input->post('item_status', TRUE)) ?  0 :  $this->input->post('item_status', TRUE);
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->output->set_content_type("application/json"); //輸出格式
        $result_output = array("error_code" => "" ,"action" => "" ,"result" => ""); //結果

        //先判斷名稱是否重覆
        $where_arr =  array(
            "name" => str_replace(" ","" , $name),
            'has_deleted' => 0
        );
        $this->basic_model->initialize( "tb_user_roles" );
        $check_has_email = $this->basic_model->get_where($where_arr);
        if( sizeof($check_has_email) > 0){
            $result_output = array("error_code" => "2" ,"action" => "add" ,"result" => "double"); //結果
            echo json_encode($result_output);
            exit();
        }

        $new_uuid = $this->uuid->v1();

        //$name - 系統管理加字元
        str_replace("系統管理","_系統管理" , $name);
        $request_data = array(
            "uuid" => $new_uuid ,
            "name" => $name ,
            "roles" =>  $roles_arr == null ? '' : $roles_arr ,
            "item_status" => $item_status ,
            "has_deleted" => 0 ,
            "create_user_uuid" => $_SESSION['manager_data']['uuid']  ,
            "modify_user_uuid" => '' ,
            "update_datetime" =>  $data['update_datetime']
        );

        $this->basic_model->initialize( "tb_user_roles" );
        $insert_id = $this->basic_model->insert($request_data);

        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "權限",
            "群組管理",
            "新增",
            "內容"
        );
        if($insert_id > 0){
            $result_output = array("error_code" => "0" ,"action" => "add" ,"result" => "success" , "id" => $insert_id); //結果
        }else{
            $result_output = array("error_code" => "1" ,"action" => "add" ,"result" => "fail" , "id" => '' ,"message" => '資料存入失敗'); //結果
        }

        echo json_encode($result_output);


        return true;
    }



    /*修改資料*/

    /**
     * @param null $gid
     */

    public function modify_get($uuid = null)
    {
        $_SESSION['session_cms_menu'] = 'a';
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => $this->unit_title,
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );
        //套件
        $custom_css = array(
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
        );

        $custom_js =array(
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/group_content.js?t='.time()),
        );

        $ext_data['uuid']  = '';

        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
//            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }

        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //取得 item guid

        if(isset($uuid) ){
            $ext_data['uuid'] =  $uuid;

            $this->basic_model->initialize( "tb_user_roles" );
            // get_all($fields = '', $where = array(), $table = '', $limit = '' , $start = '0', $order_by = '', $group_by = '')
            $where_arr = array(
                'uuid' => $uuid,
                'has_deleted' => 0
            );
            $ext_data['output_data'] = array();
            $ext_data['query_result'] = $this->basic_model->get_where($where_arr);
            foreach($ext_data['query_result'] as $row){
//                //取資料
////                echo $row->s_account;
                $ext_data['output_data']  = array(
                    'id' => $row->id,
                    'uuid' => $row->uuid,
                    'name' => $row->name,
                    'roles' => $row->roles,
                    'item_status' =>  $row->item_status,
                    'create_user_uuid' => $row->create_user_uuid,
                    'modify_user_uuid' => $row->modify_user_uuid,
                    'update_datetime' => $row->update_datetime,
                    'create_datetime' => $row->create_datetime
                );

                $this->basic_model->initialize( "tb_users" );
                $where_arr = array(
                    'uuid' => $row->modify_user_uuid
                );
                $ext_data['query_result_update_data'] = $this->basic_model->get_where($where_arr);
                $where_arr = array(
                    'uuid' => $row->create_user_uuid
                );
                $ext_data['query_result_create_data'] = $this->basic_model->get_where($where_arr);

            }
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/user_group'), 'location', 301);
        }

        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/group_content/vw_modify',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }


    /**
     * @param null $gid
     */

    public function update_item_post()
    {
        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            $result_output = array("error_code" => "0" ,"action" => "" ,"result" => "login" ); //結果
            echo json_encode($result_output);
            die();
        }
        //接收資料
        $uuid = $this->input->post('uuid', TRUE);
        $name = $this->input->post('name', TRUE);
        $name = str_replace("系統管理","_系統管理" , $name);
        $roles_arr = $this->input->post('roles_arr', TRUE);
        $item_status = is_null($this->input->post('item_status', TRUE)) ?  0 :  $this->input->post('item_status', TRUE);
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->output->set_content_type("application/json"); //輸出格式
        $result_output = array("error_code" => "" ,"action" => "" ,"result" => ""); //結果

        //先判斷名稱是否重覆
        $where_arr =  array(
            "name" => str_replace(" ","" , $name),
            'has_deleted' => 0
        );
        $this->basic_model->initialize( "tb_user_roles" );
        $check_has_email = $this->basic_model->get_where($where_arr);
        if( sizeof($check_has_email) > 0){
//            $result_output = array("error_code" => "2" ,"action" => "add" ,"result" => "double"); //結果
//            echo json_encode($result_output);
//            exit();
        }

        $where_arr =  array( "uuid" => $uuid );
        $request_data = array(
            "name" => $name ,
            "roles" => $roles_arr ,
            "item_status" => $item_status ,
            "modify_user_uuid" =>  $_SESSION['manager_data']['uuid']  ,
            "update_datetime" =>  $data['update_datetime']
        );

        $this->basic_model->initialize( "tb_user_roles" );
        $this->basic_model->update($request_data,$where_arr);

        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "權限",
            "群組管理",
            "修改",
            "內容"
        );
        $result_output = array("error_code" => "0" ,"action" => "update" ,"result" => "success" ); //結果

        echo json_encode($result_output);


        return true;
    }

}
