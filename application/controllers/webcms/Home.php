<?php
/**
 * Created by PhpStorm.
 * User: fengw
 * Date: 2018/9/20
 * Time: 下午 04:10
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('basic_model');  //選擇MODEL
        $this->load->helper('cookie');
        $this->load->helper('url');

    }
    public function index_get()
    {

        $_SESSION['session_cms_menu'] = '';
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '後臺首頁',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );
        //套件
        $custom_css = array(
            'common_style' => base_url('assets/webcms/css/all_site.css'),
        );

        $custom_js =array(
        );



        //判斷登陸狀態
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }

        //當前使用者管記錄資料
        $where_array = array(
            'user_id' =>  $_SESSION['manager_data']['uuid']
        );
        $ext_data['get_current_user_logs'] = $this->basic_model->get_all('*', $where_array,  array(),'tb_user_logs', 1,0, 'create_datetime DESC');

        //系統管理員可察看記錄
        $ext_data['get_all_user_logs'] = $this->basic_model->get_all('*',  array() ,  array(),'tb_user_logs', 5,0, 'create_datetime DESC');


        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/home/vw_index',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }

}
