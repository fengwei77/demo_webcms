<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('basic_model');  //選擇MODEL
        $this->load->model('banner_model');  //選擇MODEL
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');

    }

    public function index_get($page = 0)
    {
        $_SESSION['session_cms_menu'] = 'b';
        if(  $_SESSION['ss_pagename'] != $this->router->fetch_class()){
            unset($_SESSION['ss_sort_name']);
            unset($_SESSION['ss_sort_filed']);
            unset($_SESSION['ss_sort']);
        }
        $_SESSION['ss_pagename'] = $this->router->fetch_class();

        if(!isset( $_SESSION[$this->router->fetch_class() . '-timer'])){
            $_SESSION[$this->router->fetch_class() . '-timer'] = time();
        }else{
            $perv_time =  $_SESSION[$this->router->fetch_class() . '-timer'];
            if( (time() - $perv_time) > $this->config->item('list_fresh_time') ){
                $_SESSION[$this->router->fetch_class() . '-timer'] = time();
                header("Refresh:0");
                die();
            }
        }
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '圖片管理',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );
        //套件
        $custom_css = array(
            'bootstrap3-editable' => base_url('bower_components/bootstrap3-editable/css/bootstrap-editable.css'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.css'),
            'switch_box' => base_url('assets/webcms/css/switch_box.css'),
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
        );

        $custom_js =array(
            'bootstrap3-editable' => base_url('bower_components/bootstrap3-editable/js/bootstrap-editable.min.js'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.js'),
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/banners.js?t='.time()),
            'pagination' => base_url('assets/webcms/js/pagination.js'),

        );

        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }
        //所有資料
        $this->basic_model->initialize( "tb_banners" );

        $where_arr = array(
            'has_deleted' => 0
        );
        //所有資料
        $ext_data['search_keywords'] = str_replace( " ","", $this->input->get('search_keywords',TRUE) );
        $ext_data['search_status'] = $this->input->get('search_status',TRUE);
        $ext_data['search_slot'] = $this->input->get('search_slot',TRUE);
        if($page == 'reset'){
            unset(  $_SESSION[ $this->router->fetch_class()]  );
        }

        if(strlen($ext_data['search_keywords'])>0){
            $_SESSION[ $this->router->fetch_class()]['search_keywords'] = $ext_data['search_keywords'];
        }else{
            if(isset( $_SESSION[ $this->router->fetch_class()]['search_keywords'])){
                $ext_data['search_keywords'] =  $_SESSION[ $this->router->fetch_class()]['search_keywords'];
            }
        }

        if(strlen($ext_data['search_status']) > 0){
            $_SESSION[ $this->router->fetch_class()]['status'] = $ext_data['search_status'];
        }else{
            if(isset( $_SESSION[ $this->router->fetch_class()]['status'])){
                $ext_data['search_status'] = $_SESSION[ $this->router->fetch_class()]['status'];
            }
        }
        if(strlen($ext_data['search_slot']) > 0){
            $_SESSION[ $this->router->fetch_class()]['search_slot'] = $ext_data['search_slot'];
        }else{
            if(isset( $_SESSION[ $this->router->fetch_class()]['search_slot'])){
                $ext_data['search_slot'] = $_SESSION[ $this->router->fetch_class()]['search_slot'];
            }
        }

        $ext_data['query_all_count'] = $this->banner_model->get_banners_list_count( $ext_data['search_keywords']  , $ext_data['search_slot'], $ext_data['search_status'] );
        $page_size = 20;  //一頁顯示幾個項目
        if(isset($_SESSION['ss_page_size_name']) ==  $_SESSION['ss_pagename']){
            $page_size = $_SESSION['ss_page_size'];
        }

        //排序
        $order_by ='';

        if(isset( $_SESSION['ss_sort_name'] )){

            $order_by = $_SESSION['ss_sort_filed']." ".  $_SESSION['ss_sort'];

        }
        $ext_data['query_all'] = $this->banner_model->get_banners_list( $ext_data['search_keywords'] , $ext_data['search_slot'],$ext_data['search_status'] , $page_size ,$page,$order_by);

        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //分頁
        //region 分頁功能
        $config['base_url'] = base_url().'webcms/'. $this->router->fetch_class();
        $config['total_rows'] = $ext_data['query_all_count'];
        $config['per_page'] = $page_size;
        $config['num_links'] = 5;
        $config['uri_segment'] = 3;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '第一頁';
        $config['last_link'] = '最後一頁';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        //endregion

        $this->pagination->initialize($config);
        $ext_data['links'] = $this->pagination->create_links();

        $ext_data['page_num'] = $page;
        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/banners/vw_index',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }

    /**
     * 修改項目狀態
     * @param null $uuid
     */
    public function change_item_status_post(){
        //接收資料
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $uuid = $this->input->post('uuid', TRUE);
        $do = $this->input->post('do', TRUE);
        $current_item_status = 0;
        $current_has_deleted = 0;
        $current_show_index = 0;

        $action = '刪除';

        $where_arr =  array( "uuid" => $uuid );
        //目前狀態

        $this->basic_model->initialize( "tb_banners" );
        $ext_data['query_result'] = $this->basic_model->get_where($where_arr);
        foreach($ext_data['query_result'] as $row){
            if($row->item_status == 0){
                $current_item_status = 1;
            }
            if( $row->has_deleted == 0){
                $current_has_deleted = 1;
            }
        }

        if($do == 'delete'){
            $request_data = array(
                'has_deleted' => $current_has_deleted,
                "modify_user_uuid" =>  $_SESSION['manager_data']['uuid']  ,
                "update_datetime" =>  $data['update_datetime']
            );
        }else if($do == 'status'){
            $action = '更新狀態';
            $request_data = array(
                'item_status' =>  $current_item_status,
                "modify_user_uuid" =>  $_SESSION['manager_data']['uuid']  ,
                "update_datetime" =>  $data['update_datetime']
            );
        }

        $this->basic_model->update($request_data,$where_arr);

        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "圖片類別",
            "",
            $action,
            "內容"
        );
        $result_output = array("error_code" => "0" ,"action" => $do ,"result" => "success" ); //結果

        echo json_encode($result_output);



        return true;
    }

    /**
     * 修改項目狀態
     * @param null $uuid
     */
    public function change_order_post(){
        //接收資料
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $uuid = $this->input->post('uuid', TRUE);
        $value = intval($this->input->post('value', TRUE));

        $where_arr =  array( "uuid" => $uuid );
        //目前狀態

        $this->basic_model->initialize( "tb_banners" );
        $action = '更新排序';
        $do = 'change_number';
        $request_data = array(
            'sort_number' =>  $value,
            "modify_user_uuid" =>  $_SESSION['manager_data']['uuid']  ,
            "update_datetime" =>  $data['update_datetime']
        );

        $this->basic_model->update($request_data,$where_arr);

        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "圖片",
            "",
            $action,
            "內容"
        );
        $result_output = array("error_code" => "0" ,"action" =>  $do ,"result" => "success" ); //結果

        $like = array();


        echo 'success';
    }

}
