<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_content extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('basic_model');  //選擇MODEL
        $this->load->helper('cookie');
        $this->load->helper('url');

    }
    public function index_get()
    {
        $_SESSION['session_cms_menu'] = 'a';
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '帳號管理',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );
        //套件
        $custom_css = array(
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
        );

        $custom_js =array(
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/user_content.js'),
        );


        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }

        $ext_data['uuid']  = '';

        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //取群組選單
        $this->basic_model->initialize( "tb_user_roles" );
        $where_arr = array(
            'item_status' => 1,
            'has_deleted' => 0
        );
        $ext_data['output_data'] = array();
        $ext_data['query_user_roles_result'] = $this->basic_model->get_where($where_arr);

        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/user_content/vw_index',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }

    /*新增資料*/

    /**
     * @param null $gid
     */

    public function add_item_post()
    {
        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            $result_output = array("error_code" => "0" ,"action" => "" ,"result" => "login" ); //結果
            echo json_encode($result_output);
            die();
        }
        //接收資料

        $name = $this->input->post('name', TRUE);
        $pass = $this->input->post('pass', TRUE);
        $email = $this->input->post('email', TRUE);
        $group = $this->input->post('group', TRUE);
        $roles_arr = $this->input->post('roles_arr', TRUE);
        $access_range = $this->input->post('access_range', TRUE);
        $item_status = is_null($this->input->post('item_status', TRUE)) ?  0 :  $this->input->post('item_status', TRUE);
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->output->set_content_type("application/json"); //輸出格式
        $result_output = array("error_code" => "" ,"action" => "" ,"result" => ""); //結果
        //判斷群組是不是administrator
        $this->basic_model->initialize( "tb_user_roles" );
        $where_arr = array(
            'uuid' => $group,
            'uuid <>' => ''
        );
        $ext_data['query_user_roles_result'] = $this->basic_model->get_where($where_arr);
        $group_roles ='';
        foreach (  $ext_data['query_user_roles_result'] as $item){
            if( $item->roles  ==  'all'){
                $group_roles =  'all';
            }
        }
        if( $group_roles == 'all' ){
            $access_range = 'all';
        } else{
            $access_range =  $access_range != '' ? implode(",", $access_range) : '';
        }




        //先判斷信箱是否重覆
        $where_arr =  array(
            "email" =>$email,
            'has_deleted' => 0
        );
        $this->basic_model->initialize( "tb_users" );
        $check_has_email = $this->basic_model->get_where($where_arr);
        if( sizeof($check_has_email) > 0){
            $result_output = array("error_code" => "2" ,"action" => "add" ,"result" => "double"); //結果
            echo json_encode($result_output);
            exit();
        }

        $new_uuid = $this->uuid->v1();
        $this->encryption->initialize(
            array(
                'driver' => 'openssl',
                'cipher' => 'aes-128',
                'mode' => 'cbc'
            )
        );


        $request_data = array(
            "uuid" => $new_uuid ,
            "roles_id" => '' ,
            "roles_uuid" => $group ,
            "name" => $name ,
            "account" => $email ,
            "pass" => $this->encryption->encrypt($pass) ,
            "email" => $email ,
            "level" => 0 ,
            "access_range " => $access_range,
            "item_status" => $item_status ,
            "has_deleted" => 0 ,
            "create_user_uuid" => $_SESSION['manager_data']['uuid']  ,
            "modify_user_uuid" => '' ,
            "update_datetime" =>  $data['update_datetime']
        );

        $this->basic_model->initialize( "tb_users" );
        $insert_id = $this->basic_model->insert($request_data);

        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "權限",
            "帳號管理",
            "新增",
            "內容"
        );
        if($insert_id > 0){
            $result_output = array("error_code" => "0" ,"action" => "add" ,"result" => "success" , "id" => $insert_id); //結果
        }else{
            $result_output = array("error_code" => "1" ,"action" => "add" ,"result" => "fail" , "id" => '' ,"message" => '資料存入失敗'); //結果
        }

        echo json_encode($result_output);


        return true;
    }

    /*修改資料*/

    /**
     * @param null $gid
     */
    public function modify_get($uuid = null)
    {
        $_SESSION['session_cms_menu'] = 'a';
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '修改帳號',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );

        //套件
        $custom_css = array(
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
        );

        $custom_js =array(
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/user_content.js'),
        );


        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }

        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //取得 item guid

        if(isset($uuid) ){
            $ext_data['uuid'] =  $uuid;

            $this->basic_model->initialize( "tb_users" );
            // get_all($fields = '', $where = array(), $table = '', $limit = '' , $start = '0', $order_by = '', $group_by = '')
            $where_arr = array(
                'uuid' => $uuid,
                'has_deleted' => 0
            );

            $ext_data['output_data'] = array();
            $ext_data['query_result'] = $this->basic_model->get_where($where_arr);
            foreach($ext_data['query_result'] as $row){

//                //取資料
////                echo $row->s_account;
                $ext_data['output_data']  = array(
                    'id' => $row->id,
                    'uuid' => $row->uuid,
                    'roles_id' => $row->roles_id,
                    'roles_uuid' => $row->roles_uuid,
                    'name' => $row->name,
                    'account' => $row->account,
                    'pass' => $row->pass,
                    'email' => $row->email,
                    'level' => $row->level,
                    'access_range' => $row->access_range,
                    'item_status' =>  $row->item_status,
                    'create_user_uuid' => $row->create_user_uuid,
                    'modify_user_uuid' => $row->modify_user_uuid,
                    'update_datetime' => $row->update_datetime,
                    'create_datetime' => $row->create_datetime
                );



                $this->basic_model->initialize( "tb_users" );
                $where_arr = array(
                    'uuid' => $row->modify_user_uuid
                );
                $ext_data['query_result_update_data'] = $this->basic_model->get_where($where_arr);
                $where_arr = array(
                    'uuid' => $row->create_user_uuid
                );
                $ext_data['query_result_create_data'] = $this->basic_model->get_where($where_arr);


                //取群組選單
                $this->basic_model->initialize( "tb_user_roles" );
                $where_arr = array(
                    'item_status' => 1,
                    'has_deleted' => 0
                );
                $ext_data['query_user_roles_result'] = $this->basic_model->get_where($where_arr);
                $ext_data['roles'] ='';
                foreach (  $ext_data['query_user_roles_result'] as $item){
                    if( $item->uuid ==  $ext_data['output_data']['roles_uuid']){
                        $ext_data['roles'] = $item->roles;
                    }
                }
            }
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/user'), 'location', 301);
        }


        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/user_content/vw_modify',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }

    /**
     * @param null $gid
     */

    public function update_item_post()
    {
        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            $result_output = array("error_code" => "0" ,"action" => "" ,"result" => "login" ); //結果
            echo json_encode($result_output);
            die();
        }
        //接收資料
        $uuid = $this->input->post('uuid', TRUE);
        $name = $this->input->post('name', TRUE);
        $pass = $this->input->post('pass', TRUE);
        $email = $this->input->post('email', TRUE);
        $group = $this->input->post('group', TRUE);
        $roles_arr = $this->input->post('roles_arr', TRUE);
        $access_range = $this->input->post('access_range', TRUE);
        $item_status = is_null($this->input->post('item_status', TRUE)) ?  0 :  $this->input->post('item_status', TRUE);
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->output->set_content_type("application/json"); //輸出格式
        $result_output = array("error_code" => "" ,"action" => "" ,"result" => ""); //結果

        //判斷群組是不是administrator
        $this->basic_model->initialize( "tb_user_roles" );
        $where_arr = array(
            'uuid' => $group,
            'uuid <>' => ''
        );
        $ext_data['query_user_roles_result'] = $this->basic_model->get_where($where_arr);
        $group_roles ='';
        foreach (  $ext_data['query_user_roles_result'] as $item){
            if( $item->roles  ==  'all'){
                $group_roles =  'all';
            }
        }
        if( $group_roles == 'all' ){
            $access_range = 'all';
        } else{
            $access_range =  $access_range != '' ? implode(",", $access_range) : '';
        }


        $where_arr =  array( "uuid" => $uuid );
        $this->encryption->initialize(
            array(
                'driver' => 'openssl',
                'cipher' => 'aes-128',
                'mode' => 'cbc'
            )
        );
        if($email == 'administrator'){
            $request_data = array(
                "pass" => $this->encryption->encrypt($pass),
                "modify_user_uuid" => $_SESSION['manager_data']['uuid'],
                "update_datetime" => $data['update_datetime']
            );
        }else
        {


            $request_data = array(
                "roles_id" => '',
                "roles_uuid" => $group,
                "name" => $name,
                "account" => $email,
                "pass" => $this->encryption->encrypt($pass),
                "email" => $email,
                "level" => 0,
                "access_range " => $access_range,
                "item_status" => $item_status,
                "has_deleted" => 0,
                "modify_user_uuid" => $_SESSION['manager_data']['uuid'],
                "update_datetime" => $data['update_datetime']
            );
        }

        $this->basic_model->initialize( "tb_users" );
        $this->basic_model->update($request_data,$where_arr);

        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "權限",
            "群組管理",
            "修改",
            "內容"
        );
        $result_output = array("error_code" => "0" ,"action" => "update" ,"result" => "success" ); //結果

        echo json_encode($result_output);


        return true;
    }


}
