<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_content extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('basic_model');  //選擇MODEL
        $this->load->model('banner_model');  //選擇MODEL
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');

    }
    public function index_get()
    {

        $_SESSION['session_cms_menu'] = 'b';

        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '新增圖片內容',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );
        //套件
        $custom_css = array(
            'cropper' => base_url('bower_components/cropper/cropper.min.css'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.css'),
            'bootstrap-datetimepicker' => base_url('bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'),
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
            'switch_box' => base_url('assets/webcms/css/switch_box.css'),
        );

        $custom_js =array(
            'moment.js' => base_url('bower_components/moment.js/moment.js'),
            'moment.js-locales' => base_url('bower_components/moment.js/moment-with-locales.js'),
            'bootstrap-datetimepicker' => base_url('bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js'),
            'bootstrap-filestyle' => base_url('bower_components/bootstrap-filestyle/bootstrap-filestyle.min.js'),
            'cropper' => base_url('bower_components/cropper/cropper.min.js'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.js'),
            'image_cropper' => base_url('assets/webcms/js/image_upload_with_cropper.js'),
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/banner_content.js?t='.time()),
            'pagination' => base_url('assets/webcms/js/pagination.js'),

        );



        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }

        $ext_data['uuid']  = '';

        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);


        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/banner_content/vw_index',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }

    /*新增資料*/

    /**
     * @param null $gid
     */

    public function add_item_post()
    {
        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            $result_output = array("error_code" => "0" ,"action" => "" ,"result" => "login" ); //結果
            echo json_encode($result_output);
            die();
        }
        //接收資料

        $name = $this->input->post('name', TRUE);
        $is_link = $this->input->post('is_link', TRUE)  != null ? $this->input->post('is_link', TRUE) : '0';
        $link = $this->input->post('link', TRUE);
        $file_1 = $this->input->post('file_1', TRUE) != null ? $this->input->post('file_1', TRUE) : '';
        $file_2 = $this->input->post('file_2', TRUE) != null ? $this->input->post('file_2', TRUE) : '';
        $start_date = $this->input->post('start_date', TRUE);
        $expire_date = $this->input->post('expire_date', TRUE);
        $sort_number = $sort_number = is_null($this->input->post('sort_number', TRUE)) ?  0 :  $this->input->post('sort_number', TRUE);
        $item_status = $item_status = is_null($this->input->post('item_status', TRUE)) ?  0 :  $this->input->post('item_status', TRUE);
        $has_deleted = $has_deleted = is_null($this->input->post('has_deleted', TRUE)) ?  0 :  $this->input->post('has_deleted', TRUE);
        $create_user_uuid = $_SESSION['manager_data']['uuid'] ;
        $modify_user_uuid = '';


        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->output->set_content_type("application/json"); //輸出格式
        $result_output = array("error_code" => "" ,"action" => "" ,"result" => ""); //結果


        $new_uuid = $this->uuid->v1();
        $request_data = array(
            'uuid' => $new_uuid ,
            'name' => $name ,
            'link' => $link ,
            'is_link' => $is_link,
            'link' => $link,
            'file_1' => $file_1,
            'file_2' => $file_2,
            'start_date' => $start_date ,
            'expire_date' => $expire_date ,
            'sort_number' => $sort_number ,
            'item_status' => $item_status ,
            'has_deleted' => $has_deleted ,
            'create_user_uuid' => $create_user_uuid ,
            'modify_user_uuid' => $modify_user_uuid ,
            'update_datetime' =>  $data['update_datetime']  ,
        );

        $this->basic_model->initialize( "tb_banners" );
        $insert_id = $this->basic_model->insert($request_data);


        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "版位內容",
            "版位管理",
            "新增",
            "內容"
        );
        if($insert_id > 0){
            $result_output = array("error_code" => "0" ,"action" => "add" ,"result" => "success" , "id" => $insert_id); //結果
        }else{
            $result_output = array("error_code" => "1" ,"action" => "add" ,"result" => "fail" , "id" => '' ,"message" => '資料存入失敗'); //結果
        }

        echo json_encode($result_output);


        return true;
    }

    /*修改資料*/

    /**
     * @param null $gid
     */
    public function modify_get($uuid = null)
    {
        $_SESSION['session_cms_menu'] = 'b';

        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '修改版位內容',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );
        //套件
        //套件
        $custom_css = array(
            'cropper' => base_url('bower_components/cropper/cropper.min.css'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.css'),
            'bootstrap-datetimepicker' => base_url('bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'),
            'all_site_css' => base_url('assets/webcms/css/all_site.css'),
            'switch_box' => base_url('assets/webcms/css/switch_box.css'),
        );

        $custom_js =array(
            'moment.js' => base_url('bower_components/moment.js/moment.js'),
            'moment.js-locales' => base_url('bower_components/moment.js/moment-with-locales.js'),
            'bootstrap-datetimepicker' => base_url('bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js'),
            'bootstrap-filestyle' => base_url('bower_components/bootstrap-filestyle/bootstrap-filestyle.min.js'),
            'cropper' => base_url('bower_components/cropper/cropper.min.js'),
            'fancybox' => base_url('bower_components/fancybox/jquery.fancybox.min.js'),
            'image_cropper' => base_url('assets/webcms/js/image_upload_with_cropper.js'),
            'all_site_js' => base_url('assets/webcms/js/all_site.js'),
            'this_js' => base_url('assets/webcms/js/banner_content.js?t='.time()),
            'pagination' => base_url('assets/webcms/js/pagination.js'),

        );



        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
        }



        //選單
        $this->config->load('menu_setting', TRUE);
        $ext_data['side_menu'] = $this->config->item('side_menu', 'menu_setting');
        $ext_data['sidebar_menu'] =   $this->load->view('webcms/sidebar_menu',$ext_data, TRUE);

        //取得 item guid
        if(isset($uuid) ){
            $ext_data['uuid'] =  $uuid;

            $this->basic_model->initialize( "tb_banners" );
            // get_all($fields = '', $where = array(), $table = '', $limit = '' , $start = '0', $order_by = '', $group_by = '')
            $where_arr = array(
                'uuid' => $uuid,
                'has_deleted' => 0
            );

            $ext_data['output_data'] = array();
            $ext_data['query_result'] = $this->basic_model->get_where($where_arr);
            foreach($ext_data['query_result'] as $row){

//                //取資料
////                echo $row->s_account;
                $ext_data['output_data']  = array(
                    'id' => $row->id,
                    'uuid' => $row->uuid,
                    'name' => $row->name,
                    'is_link' => $row->is_link,
                    'link' => $row->link,
                    'file_1' => $row->file_1,
                    'file_2' => $row->file_2,
                    'start_date' => $row->start_date,
                    'expire_date' => $row->expire_date,
                    'sort_number' => $row->sort_number,
                    'item_status' => $row->item_status,
                    'has_deleted' => $row->has_deleted,
                    'create_user_uuid' => $row->create_user_uuid,
                    'modify_user_uuid' => $row->modify_user_uuid,
                    'update_datetime' => $row->update_datetime,
                    'create_datetime' => $row->create_datetime,
                );




                //使用者記錄
                $this->basic_model->initialize( "tb_users" );
                $where_arr = array(
                    'uuid' => $row->modify_user_uuid
                );
                $ext_data['query_result_update_data'] = $this->basic_model->get_where($where_arr);
                $where_arr = array(
                    'uuid' => $row->create_user_uuid
                );
                $ext_data['query_result_create_data'] = $this->basic_model->get_where($where_arr);


            }

        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/banner'), 'location', 301);
        }


        //內容
        $ext_data['page_content'] =   $this->load->view('webcms/banner_content/vw_modify',$ext_data, TRUE);

        $this->load_cms_template($custom_js,$custom_css,null,$ext_data);

    }


    /*更新資料*/
    /**
     * @param null $gid
     */
    public function update_item_post()
    {
        //管理員資料
        if(isset( $_SESSION['manager_data'])){
            $ext_data['manager_data'] =  $_SESSION['manager_data'];
        }else{
            redirect(base_url($this->config->item('webcms_controllers_folder').'/login/logout'), 'location', 301);
            die();
        }
        //接收資料
        $uuid = $this->input->post('uuid', TRUE);

        $name = $this->input->post('name', TRUE);
        $is_link = $this->input->post('is_link', TRUE)  != null ? $this->input->post('is_link', TRUE) : '0';
        $link = $this->input->post('link', TRUE);
        $file_1 = $this->input->post('file_1', TRUE) != null ? $this->input->post('file_1', TRUE) : '';
        $file_2 = $this->input->post('file_2', TRUE) != null ? $this->input->post('file_2', TRUE) : '';
        $start_date = $this->input->post('start_date', TRUE);
        $expire_date = $this->input->post('expire_date', TRUE);
        $sort_number = $sort_number = is_null($this->input->post('sort_number', TRUE)) ?  0 :  $this->input->post('sort_number', TRUE);
        $item_status = $item_status = is_null($this->input->post('item_status', TRUE)) ?  0 :  $this->input->post('item_status', TRUE);
        $has_deleted = $has_deleted = is_null($this->input->post('has_deleted', TRUE)) ?  0 :  $this->input->post('has_deleted', TRUE);
        $create_user_uuid = $_SESSION['manager_data']['uuid'] ;

        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->output->set_content_type("application/json"); //輸出格式
        $result_output = array("error_code" => "" ,"action" => "" ,"result" => ""); //結果



        $where_arr =  array( "uuid" => $uuid );

        $request_data = array(
            'name' => $name ,
            'link' => $link ,
            'is_link' => $is_link,
            'link' => $link,
            'start_date' => $start_date ,
            'expire_date' => $expire_date ,
            'sort_number' => $sort_number ,
            'item_status' => $item_status ,
            'has_deleted' => $has_deleted ,
            "create_user_uuid" => $_SESSION['manager_data']['uuid']  ,
            "modify_user_uuid" => '' ,
            "update_datetime" =>  $data['update_datetime']
        );
        if($file_1!= '')
        {
            $request_data['file_1'] = $file_1;
        }
        if($file_2!= '')
        {
            $request_data['file_2'] = $file_2;
        }
        $this->basic_model->initialize( "tb_banners" );
        $update_id = $this->basic_model->update($request_data,$where_arr);


        //記錄更新LOG
        $this->basic_model->rec_user_logs(
            $_SESSION['manager_data']['uuid'],
            $_SESSION['manager_data']['account'],
            $_SESSION['manager_data']['name'],
            $_SESSION['manager_data']['group_name'],
            "版位內容",
            "版位管理",
            "修改",
            "內容"
        );
        $result_output = array("error_code" => "0" ,"action" => "update" ,"result" => "success" ); //結果

        echo json_encode($result_output);


        return true;
    }







}
