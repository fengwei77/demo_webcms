<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: fengw
 * Date: 2018/9/17
 * Time: 上午 09:33
 */

class Login extends MY_Controller {

    private static $encryptionKey = '';//32位
    private static $encryptionIV = '';//16位

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');  //選擇MODEL
        $this->load->helper('cookie');
        $this->load->helper('url');
        SELF::$encryptionKey = $this->config->item('encryptionKey');
        SELF::$encryptionIV = $this->config->item('encryptionIV');
    }
    public function index_get()
    {

//          $ciphertext =  $this->crypt->encryptdata("administrator");
//          echo $this->crypt->decryptdata('UkhuZTBBajNWTE5YcHQweUdFU2tSNTFxWVZ1QklXSGhDQytsYmpFU3M5Yz0=') ;
//         echo  $this->crypt->decryptdata($ciphertext);

        $_SESSION['session_cms_menu'] = '';
        $ext_data = array(
            'session_menu' =>  $_SESSION['session_cms_menu'] ,
            'unit_title' => '檢視群組',
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_side_title'  =>  $this->config->item('webcms_side_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'webcms_controllers_folder' =>  $this->config->item('webcms_controllers_folder'),
            'webcms_permissions' =>  $this->config->item('webcms_permissions'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash(),
            'logout' =>   $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login/logout'
        );

        //管理者初始化
        $this->admin_model->init_sys_manager();


        $data = array(
            'webcms_title' =>  $this->config->item('webcms_title'),
            'webcms_style' =>  $this->config->item('webcms_style'),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'csrf_hash' => $this->security->get_csrf_hash()
        );
        $data = array_merge($data,$ext_data);
        // you really don't need the line below
        // $data = $this->data;
        $this->data['error'] = '';

        $this->load->view( $this->config->item('webcms_folder').'login/vw_index',$data);
    }

    public function check_post(){
        $output = array('result' => ''); //結果
        //接收資料
        $data['account'] = $this->input->post('account', TRUE);
        $data['password'] = $this->input->post('password', TRUE);
        //echo $data['account'];
        $check_01 = $this->admin_model->check_manager_auth( $data['account'],$data['password']);
        if($check_01['result'] == 'success'){
            $_SESSION['manager_data'] = array(
                'uuid' =>  $check_01['user_uuid'],
                'status' => 'passed',
                'account' =>  $check_01['account'],
                'email' =>  $check_01['email'],
                'name' =>  $check_01['name'],
                'group_name' =>  $check_01['roles_name'],
                'group_roles' =>  $check_01['roles'],
                'access_range' =>  $check_01['access_range'] ,
            );

            log_message('info',   $data['account'] . ' - 登入後台成功 => ' . $this->input->ip_address());
        }else{
            log_message('info',   $data['account'] . ' - 登入後台失敗 => ' . $this->input->ip_address());
        }


        $output = array('result' => $check_01['result']); //結果
        $this->response($output, 200); // 200 being the HTTP response code


    }

    public function logout_get()	{

        unset(
            $_SESSION['manager_data']
        );

        $url =  $this->config->item('base_url') . $this->config->item('webcms_controllers_folder') . 'login';
        header("Location:". $url);

    }
}
