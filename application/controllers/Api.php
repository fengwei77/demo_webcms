<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->model('basic_model');  //選擇MODEL
        $this->load->model('banner_model');  //選擇MODEL
        $this->load->model('event_model');  //選擇MODEL
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');

    }
    public function index()
    {
       echo '豆漿！！！';
    }


    /**
     *輸出首頁主圖JSON
     */
    public function banners_get($qty){
        $nowtime = time();
        $order_by = "sort_number DESC , create_datetime DESC";
        $where = $nowtime." >=  unix_timestamp(`start_date`) AND  ".$nowtime." <=  unix_timestamp(`expire_date`)  ";
        $query_result = $this->banner_model->front_get_banners_list(  $where  ,'' ,   '1' , $qty , 0,  $order_by);
        echo json_encode($query_result);
    }


    /**
     *輸出首頁主圖JSON
     */
    public function events_get($qty){
        $nowtime = time();
        $order_by = "sort_number DESC , start_date DESC";
        $where = $nowtime." >=  unix_timestamp(`start_date`) AND  ".$nowtime." <=  unix_timestamp(`expire_date`)  ";
        $query_result = $this->event_model->get_article_list( $where, '' , 1 , $qty ,0, $order_by );

        echo json_encode($query_result);
    }

}
