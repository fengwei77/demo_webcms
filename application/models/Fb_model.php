<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Fb_model extends CI_Model {

    protected $table_name = '';
    protected $primary_key = 'id';
    private $MASTER;
    private $SALVE;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->MASTER = $this->load->database('master', TRUE);
        $this->SALVE = $this->load->database('slave', TRUE);
    }

    public function initialize($tbl) {
        $this->table_name = $tbl;
    }

    public function get_table_name() {
        return  $this->table_name ;
    }


    //region 圖片管理
    /**
     * @param string $keywords
     * @param string $slot
     * @param string $status
     * @param string $limit
     * @param string $start
     * @return array
     */
    function get_fb_list($where = array() ,$keywords = ''  ,$status = '' , $limit = '' , $start = '0', $order_by = ''){
        $data = array();
        $this->SALVE->select("*");
        $this->SALVE->from("tb_fb");
        $this->SALVE->where("has_deleted = 0");
        if (count($where)>0 && $where != '') {
            $this->SALVE->where($where);
        }
        if($keywords != ''){
            $this->SALVE->where("fb_name like '%".$keywords."%'");
        }
        if($status != ''){
            $this->SALVE->where("item_status = '".$status."'");
        }

        if ($limit != '') {
            $this->SALVE->limit($limit,$start);
        }

        if($order_by != ''){
            $this->SALVE->order_by($order_by);
        }else{
            $this->SALVE->order_by("create_datetime","DESC");

        }
        $Q = $this->SALVE->get();

//        echo  $this->SALVE->last_query();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }

    /**
     * @param string $keywords
     * @param string $slot
     * @param string $status
     * @return mixed
     */
    function get_fb_list_count($where = array() ,$keywords = ''   ,$status = '' ){
        $this->SALVE->select("id");
        $this->SALVE->from("tb_fb");
        $this->SALVE->where("has_deleted = 0");
        if (count($where)>0 && $where != '') {
            $this->SALVE->where($where);
        }
        if($keywords != ''){
            $this->SALVE->where("fb_name like '%".$keywords."%'");
        }
        if($status != ''){
            $this->SALVE->where("item_status = '".$status."'");
        }

        $data = $this->SALVE->count_all_results();

        return $data;
    }


}