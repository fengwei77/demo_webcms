<?php
class Admin_model extends CI_Model
{
    protected $table_name = '';
    protected $primary_key = 'id';
    private $MASTER;
    private $SALVE;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->MASTER = $this->load->database('master', TRUE);
        $this->SALVE = $this->load->database('slave', TRUE);
    }


    public function initialize($tbl) {
        $this->table_name = $tbl;
    }

    function check_manager_auth($account , $password)   //判斷管理員帳密,回傳TRUE FALSE
    {
        $result = array('result'=>'fail');
        $roles_array = array();
        $array = array("account" =>  $this->crypt->encryptdata($account));
        $query = $this->SALVE->get_where('tb_users', $array);

        foreach($query->result() as $item){
                if( $this->crypt->decryptdata($item->pass) === $password ){
                    if($item->item_status == 1) {
                        $result['result'] = 'success';
                        $user_array = array(
                            'user_uuid' =>  $item->uuid,
                            'name' => $this->crypt->decryptdata($item->name),
                            'account' => $this->crypt->decryptdata($item->account ),
                            'email' => $this->crypt->decryptdata($item->email),
                            'access_range' => $this->crypt->decryptdata($item->access_range)
                        );
                        $result = array_merge($result,$user_array);
                        //取群組
                        $roles_uuid = $item->roles_uuid;
                        $array = array('uuid' => $roles_uuid);
                        $query_roles = $this->SALVE->get_where('tb_user_roles', $array);
                        foreach($query_roles->result() as $item_r){
                            if($item_r->item_status == 1 && $item_r->has_deleted == 0){
                                $roles_array = array('roles_name' => $item_r->name,'roles_name' => $item_r->name,'roles' => $item_r->roles , 'roles_item_status' => $item_r->item_status );

                            }else{
                                $result['result'] = 'denied';

                            }
                        }

                        $result = array_merge($result,$roles_array);

                        //紀錄登錄記錄
                        //純進入後台不紀錄

                    }else if($item->item_status == 0){
                        $result['result'] =  'denied';
                    }else if($item->has_deleted == 1){
                        $result['result'] = 'denied';
                    }

                }

        }
        return  $result;
    }

    function init_sys_manager() //初始使用者
    {

        $this->SALVE->where('account',  $this->crypt->encryptdata('administrator'));
        $this->SALVE->from('tb_users');

        $count_result = $this->SALVE->count_all_results();
        if ($count_result == 0)
        {
            $new_uuid = $this->uuid->v1();
            $roles_uuid =$this->uuid->v1();
            $data['update_datetime'] = date('Y-m-d H:i:s');
            $data_array = array(
                'uuid' => $new_uuid,
                'roles_id' => '0',
                'roles_uuid' => $roles_uuid,
                'name' =>   $this->crypt->encryptdata('系統管理員'),
                'account' => $this->crypt->encryptdata('administrator'),
                'pass' => $this->crypt->encryptdata('0000'),
                'email' => $this->crypt->encryptdata('administrator'),
                'level' => $this->crypt->encryptdata('999'),
                'access_range' => $this->crypt->encryptdata('all'),
                'item_status' => 1,
                'has_deleted' => 0,
                'create_user_uuid' => '',
                'modify_user_uuid' => '',
                'update_datetime' =>  $data['update_datetime']
            );
//        $data['update_from_ip'] = $this->input->ip_address();

            $insert_user = $this->MASTER->insert('tb_users', $data_array);

            $data_array = array(
                'uuid' => $roles_uuid,
                'name' => '後台系統管理',
                'roles' => 'all',
                'item_status' => 1,
                'has_deleted' => 0,
                'update_datetime' =>  $data['update_datetime']
            );
            $insert_user_roles = $this->MASTER->insert('tb_user_roles', $data_array);

            log_message('info','第一次使用後台並初始化管理員,使用者IP=>'.$this->input->ip_address());
        }
    }

    //取得資料by Field with order
    function get_custom_items_orderby($table, $where_array ,$ord)
    {
        $this->SALVE->where($where_array);
        $this->SALVE->order_by($ord);
        $query = $this->SALVE->get($table);
        return $query->result();
    }

    //新增資料
    public function insert($data) {
        $data['update_datetime'] = date('Y-m-d H:i:s');
//        $data['update_from_ip'] = $this->input->ip_address();

        $success = $this->MASTER->insert($this->table_name, $data);
        if ($success) {
            return $this->MASTER->insert_id();
        } else {
            return FALSE;
        }
    }

    //刪除資料
    function delete_items($table, $guid )
    {
        $data = array('n_delete' => 1 );
        $this->MASTER->where('s_guid', $guid);
        $this->MASTER->update($table, $data);
    }

    //修改資料by Field
    function update_custom_items($table, $where_array , $array)
    {
        $this->MASTER->where($where_array);
        $this->MASTER->update($table, $array);
    }

    function get_access_range_id($uuid = ''){

        $access_range = array();
        $side_menu = $this->config->item('side_menu', 'menu_setting');
        $this->basic_model->initialize( "tb_users" );
        // get_all($fields = '', $where = array(), $table = '', $limit = '' , $start = '0', $order_by = '', $group_by = '')
        $where_arr = array(
            'uuid' => $uuid,
            'has_deleted' => 0
        );

        $output_data = array();
        $query_result= $this->basic_model->get_where($where_arr);
        foreach($query_result  as  $row){
            //取資料
            $output_data  = array(
                'id' => $row->id,
                'uuid' => $row->uuid,
                'roles_id' => $row->roles_id,
                'roles_uuid' => $row->roles_uuid,
                'name' =>  $this->crypt->decryptdata( $row->name),
                'account' =>  $this->crypt->decryptdata( $row->account),
                'pass' => $this->crypt->decryptdata(  $row->pass),
                'email' =>  $this->crypt->decryptdata( $row->email),
                'level' =>  $this->crypt->decryptdata( $row->level),
                'access_range' =>   $this->crypt->decryptdata($row->access_range),
                'item_status' =>  $row->item_status,
                'create_user_uuid' => $row->create_user_uuid,
                'modify_user_uuid' => $row->modify_user_uuid,
                'update_datetime' => $row->update_datetime,
                'create_datetime' => $row->create_datetime
            );
//        echo $output_data['access_range'];

            $roles = '';
            if($output_data['access_range'] == 'all'){
                foreach ($side_menu as $rows_0)
                {

                    $roles .= $rows_0['id'].'-view,';
                    $roles .= $rows_0['id'].'-add,';
                    $roles .= $rows_0['id'].'-del,';
                    $roles .= $rows_0['id'].'-mod,';
                    if (isset($rows_0['sub_menu'])){
                        foreach ($rows_0['sub_menu'] as $rows_1)
                        {
                            $roles .= $rows_1['id'].'-view,';
                            $roles .= $rows_1['id'].'-add,';
                            $roles .= $rows_1['id'].'-del,';
                            $roles .= $rows_1['id'].'-mod,';
                        }

                    }
                }
                $output_data['access_range'] = substr($roles,0,-1);
            }
            $access_range  =  explode(",",  str_replace(" ","",$output_data['access_range']));

        }

        return $access_range;
    }

}