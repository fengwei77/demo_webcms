<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Basic_model extends CI_Model {

    protected $table_name = '';
    protected $primary_key = 'id';
    private $MASTER;
    private $SALVE;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->MASTER = $this->load->database('master', TRUE);
        $this->SALVE = $this->load->database('slave', TRUE);
    }

    public function initialize($tbl) {
        $this->table_name = $tbl;
    }

    public function get_table_name() {
        return  $this->table_name ;
    }

    public function get($id) {
        return $this->SALVE->get_where($this->table_name, array($this->primary_key => $id))->row();
    }

    public function get_all($fields = '', $where = array(),  $like = array(), $table = '', $limit = '' , $start = '0', $order_by = '', $group_by = '') {
        $data = array();
        if ($fields != '') {
            $this->SALVE->select($fields);
        }

        if (count($where)) {
            $this->SALVE->where($where);
        }

        if (count($like)) {
            $this->SALVE->where($like);
        }

        if ($table != '') {
            $this->table_name = $table;
        }

        if ($limit != '') {
            $this->SALVE->limit($limit,$start);
        }

        if ($order_by != '') {
            $this->SALVE->order_by($order_by);
        }

        if ($group_by != '') {
            $this->SALVE->group_by($group_by);
        }

        $Q = $this->SALVE->get($this->table_name);

        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }

    public function get_all_join($fields = '',  $join= array() ,$where = array(), $like = array(), $table = '', $limit = '' , $start = '0', $order_by = '', $group_by = '') {
        $data = array();
        if ($fields != '') {
            $this->SALVE->select($fields);
        }

        if (count($join)) {
            $this->SALVE->join($join[0], $join[1]);
        }

        if (count($where)) {
            $this->SALVE->where($where);
        }

        if ($like != '') {
            $this->SALVE->where($like);
        }

        if ($table != '') {
            $this->table_name = $table;
        }

        if ($limit != '') {
            $this->SALVE->limit($limit,$start);
        }

        if ($order_by != '') {
            $this->SALVE->order_by($order_by);
        }

        if ($group_by != '') {
            $this->SALVE->group_by($group_by);
        }

        $Q = $this->SALVE->get($this->table_name);

        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }

    public function get_all_count( $where = array(),  $like = '',$table = '') {
        $data = array();
        if (count($where)) {
            $this->SALVE->where($where);
        }
        if ($like != '') {
            $this->SALVE->where($like);
        }

        if ($table != '') {
            $this->table_name = $table;
        }

        $this->SALVE->from($this->table_name);

        $data = $this->SALVE->count_all_results();

        return $data;
    }

    public function insert($data) {
        $data['update_datetime'] = date('Y-m-d H:i:s');
//        $data['update_from_ip'] = $this->input->ip_address();

        $success = $this->MASTER->insert($this->table_name, $data);
        if ($success) {
            return $this->MASTER->insert_id();
        } else {
            return FALSE;
        }
    }

    public function update($data, $where) {
        $data['update_datetime'] = date('Y-m-d H:i:s');
//        $data['update_from_ip'] = $this->input->ip_address();
        $this->MASTER->where($where);
        return $this->MASTER->update($this->table_name, $data);
    }

    function increse_field_by_1($fieldToIncrease ,$where = array())
    {
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $this->MASTER->set($fieldToIncrease, $fieldToIncrease .'+1', FALSE);
        $this->MASTER->set('`update_datetime`',$data['update_datetime']);
        $this->MASTER->where($where);
        $this->MASTER->update($this->table_name);

    }

    public function delete($id) {
        $this->MASTER->where($this->primary_key, $id);

        return $this->MASTER->delete($this->table_name);
    }

    //刪除資料by Field
    function del_where( $where_array )
    {
        $this->MASTER->where($where_array);
        return $this->MASTER->delete($this->table_name);
    }

    //取得資料by Field
    function get_where( $where_array )
    {
        $this->SALVE->where($where_array);
        $query = $this->SALVE->get($this->table_name);
        return $query->result();
    }

    /**
     * @param $user_uuid 使用者uuid
     * @param $user_name 使用者名稱
     * @param $unit 什麼單元
     * @param $item 什麼項目
     * @param $action  什麼動作
     * @param $something 內容
     * 名稱 : 公關 B 君
     * 時間 : 2017 / 7/ 19 18:42
     * 動作 單元/項目 : 刪除 每日新聞 / 內容上稿管理 內容
     */
    public function rec_user_logs($user_uuid,$user_account,$user_name,$group_name,$unit,$item,$action ,$something){
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $request_data = array(
            "ipaddress" =>  $this->input->ip_address() ,
            "roles_id" => 0 ,
            "roles_uuid" => 0 ,
            "user_id" => $user_uuid  ,
            "user_account" => $user_account  ,
            "user_name" => $user_name  ,
            "group_name" => $group_name  ,
            "unit" => $unit ,
            "item" => $item ,
            "action" => $action ,
            "something" =>  $something ,
            "sort_number" => 0 ,
            "item_status" => 1 ,
            "has_deleted" => 0 ,
            "update_datetime" =>  $data['update_datetime']
        );

        $this->basic_model->initialize( "tb_user_logs" );
        return $this->basic_model->insert($request_data);
    }

    public function get_all_keywords($fields = '', $where = array(), $table = '', $limit = '' , $start = '0', $order_by = '', $group_by = '') {
        $data = array();
        if ($fields != '') {
            $this->SALVE->select($fields);
        }

        if (count($where)) {
            $this->SALVE->where($where);
        }


        if ($table != '') {
            $this->table_name = $table;
        }

        if ($limit != '') {
            $this->SALVE->limit($limit,$start);
        }

        if ($order_by != '') {
            $this->SALVE->order_by($order_by);
        }

        if ($group_by != '') {
            $this->SALVE->group_by($group_by);
        }

        $Q = $this->SALVE->get($this->table_name);

        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }


}