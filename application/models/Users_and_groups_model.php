<?php
/**
 * Created by PhpStorm.
 * User: fengw
 * Date: 2018/9/21
 * Time: 上午 11:05
 */
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Users_and_groups_model extends CI_Model {

    protected $table_name = '';
    protected $primary_key = 'id';
    private $MASTER;
    private $SALVE;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->MASTER = $this->load->database('master', TRUE);
        $this->SALVE = $this->load->database('slave', TRUE);
    }

    public function initialize($tbl) {
        $this->table_name = $tbl;
    }

    public function get_table_name() {
        return  $this->table_name ;
    }

    /**
     * @param $user_uuid 使用者uuid
     * @param $user_name 使用者名稱
     * @param $unit 什麼單元
     * @param $item 什麼項目
     * @param $action  什麼動作
     * @param $something 內容
     * 名稱 : 公關 B 君
     * 時間 : 2017 / 7/ 19 18:42
     * 動作 單元/項目 : 刪除 每日新聞 / 內容上稿管理 內容
     */
    public function rec_user_logs($user_uuid,$user_account,$user_name,$group_name,$unit,$item,$action ,$something){
        $data['update_datetime'] = date('Y-m-d H:i:s');
        $request_data = array(
            "ipaddress" =>  $this->input->ip_address() ,
            "roles_id" => 0 ,
            "roles_uuid" => 0 ,
            "user_id" => $user_uuid  ,
            "user_account" => $user_account  ,
            "user_name" => $user_name  ,
            "group_name" => $group_name  ,
            "unit" => $unit ,
            "item" => $item ,
            "action" => $action ,
            "something" =>  $something ,
            "sort_number" => 0 ,
            "item_status" => 1 ,
            "has_deleted" => 0 ,
            "update_datetime" =>  $data['update_datetime']
        );

        $this->basic_model->initialize( "tb_user_logs" );
        $insert_id = $this->basic_model->insert($request_data);
    }

    //region 群組列表

    /**
     * @param string $keywords
     * @param string $source
     * @param string $category
     * @return bool
     */
    function get_group_list($keywords = '' ,$status = '' , $limit = '' , $start = '0'){
        $data = array();
        $this->db->select("a.id as a_id , a.uuid as a_uuid , a.name   as a_name , a.item_status   as a_item_status  ");
        $this->db->from("tb_user_roles a");
        $this->db->where("a.has_deleted = 0");
        $this->db->where("a.roles <> 'all'");
        $this->db->where("( a.name like '%".$keywords."%' )");

        if($status != ''){
            $this->db->where("a.item_status = '".$status."'");
        }

        if ($limit != '') {
            $this->db->limit($limit,$start);
        }

        $this->db->order_by("a.id","DESC");
        $Q = $this->db->get();

        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }

    /**
     * @param string $keywords
     * @param string $status
     * @return mixed
     */
    function get_group_list_count($keywords = '' ,$status = '' ){
        $this->db->select("a.id as a_id , a.uuid as a_uuid , a.name as a_name , a.item_status  as a_item_status ");
        $this->db->from("tb_user_roles a");
        $this->db->where("a.has_deleted = 0");
        $this->db->where("a.roles <> 'all'");
        $this->db->where("( a.name like '%".$keywords."%' )");

        if($status != ''){
            $this->db->where("a.item_status = '".$status."'");
        }

        $data = $this->db->count_all_results();

        return $data;
    }

    //region 帳號列表
    /**
     * @param string $keywords
     * @param string $source
     * @param string $category
     * @return bool
     */
    function get_user_list($keywords = '' ,$status = '' , $limit = '' , $start = '0'){
        $data = array();
        $this->db->select("a.id as a_id , a.uuid as a_uuid , a.name   as a_name , a.email   as a_email , a.account   as a_account , a.item_status   as a_item_status , b.uuid  as b_uuid , b.name  as b_name  ");
        $this->db->from("tb_users a");
        $this->db->join("tb_user_roles b", "b.uuid=a.roles_uuid", "left");
        $this->db->where("a.has_deleted = 0");
        $this->db->where("( a.name like '%".$keywords."%' or  a.email like '%".$keywords."%' or  b.name like '%".$keywords."%')");

        if($status != ''){
            $this->db->where("a.item_status = '".$status."'");
        }

        if ($limit != '') {
            $this->db->limit($limit,$start);
        }

        $this->db->order_by("a.id","DESC");
        $Q = $this->db->get();

        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }

    /**
     * @param string $keywords
     * @param string $status
     * @return mixed
     */
    function get_user_list_count($keywords = '' ,$status = '' ){
        $this->db->select("a.id as a_id , a.uuid as a_uuid , a.name as a_name , a.item_status  as a_item_status , b.uuid  as b_uuid , b.name  as b_name ");
        $this->db->from("tb_users a");
        $this->db->join("tb_user_roles b", "b.uuid=a.roles_uuid", "left");
        $this->db->where("a.has_deleted = 0");
        $this->db->where("( a.name like '%".$keywords."%' or  a.email like '%".$keywords."%' or  b.name like '%".$keywords."%')");

        if($status != ''){
            $this->db->where("a.item_status = '".$status."'");
        }

        $data = $this->db->count_all_results();

        return $data;
    }

}