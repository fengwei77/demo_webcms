<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Event_model extends CI_Model {

    protected $table_name = '';
    protected $primary_key = 'id';
    private $MASTER;
    private $SALVE;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->load->database();
        $this->load->helper('cookie');
        $this->load->helper('url');
        $this->load->library('pagination');
        $this->MASTER = $this->load->database('master', TRUE);
        $this->SALVE = $this->load->database('slave', TRUE);
    }

    public function initialize($tbl) {
        $this->table_name = $tbl;
    }

    public function get_table_name() {
        return  $this->table_name ;
    }


    //region 文章管理
    /**
     * @param string $keywords
     * @param string $status
     * @param string $limit
     * @param string $start
     * @return array
     */
    function get_article_list($where = array() ,$keywords = '' ,$status = '' , $limit = '' , $start = '0' ,$order_by = ''){
        $data = array();
        $this->SALVE->select("
        a.start_date as a_start_date,
        a.expire_date as a_expire_date,
        a.create_datetime as a_create_datetime,
        a.item_status as a_item_status ,
        a.id as a_id,
        a.uuid as a_uuid,
        a.title as a_title,
        a.short_title as a_short_title,
        a.content_1 as a_content_1,
        a.files as a_files,
        a.list_pic as a_list_pic,
        a.sort_number as a_sort_number
        ");
        $this->SALVE->from("tb_articles a");
        $this->SALVE->where("a.has_deleted = 0");

        if (count($keywords)>0 && $keywords != '') {
            $this->SALVE->where("a.title like '%".$keywords."%'");
            $this->SALVE->or_where("a.short_title like '%".$keywords."%'");
            $this->SALVE->or_where("a.content_1 like '%".$keywords."%'");
        }
        if (count($where)>0 && $where != '') {
            $this->SALVE->where($where);
        }

        if($status != ''){
            $this->SALVE->where("a.item_status = '".$status."'");
        }

        if ($limit != '') {
            $this->SALVE->limit($limit,$start);
        }

        if($order_by != ''){
            $this->SALVE->order_by($order_by);
        }else{
            $this->SALVE->order_by("a.create_datetime","DESC");

        }
        $Q = $this->SALVE->get();
//        echo $this->SALVE->last_query();
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }

    /**
     * @param string $keywords
     * @param string $status
     * @return mixed
     */
    function get_article_list_count($where = array() ,$keywords = '' ,$status = ''){

        $this->SALVE->select("  a.start_date as a_start_date, a.expire_date as a_expire_date,a.id as id");
        $this->SALVE->from("tb_articles a");
        $this->SALVE->where("a.has_deleted = 0");

        if (count($keywords)>0 && $keywords != '') {
            $this->SALVE->where("a.title like '%".$keywords."%'");
            $this->SALVE->or_where("a.short_title like '%".$keywords."%'");
            $this->SALVE->or_where("a.content_1 like '%".$keywords."%'");
        }
        if (count($where)>0 && $where != '') {
            $this->SALVE->where($where);
        }

        if($status != ''){
            $this->SALVE->where("a.item_status = '".$status."'");
        }

        $data = $this->SALVE->count_all_results();

        return $data;
    }

}