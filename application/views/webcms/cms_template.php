<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $ext_data['webcms_title']; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/bootstrap/dist/css/bootstrap.min.css" );?>" >
    <!-- bootstrap-switch -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css" );?>" >
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/font-awesome/css/font-awesome.min.css" );?>" >
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/Ionicons/css/ionicons.min.css" );?>" >
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/adminlte/dist/css/AdminLTE.min.css" );?>" >
    <!-- AdminLTE Skins. Choose a skin from the css/skins -->
    <link rel="stylesheet" href="<?php echo base_url('bower_components/adminlte/dist/css/skins/_all-skins.css'); ?>">
    <!-- toastr -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/toastr/toastr.min.css" );?>" >
    <!-- parsley -->
    <link rel="stylesheet" href="<?php echo base_url('bower_components/parsley/parsley.css'); ?>">
    <!-- custom css -->
    <?php
    if(isset( $opt_css)){
        foreach ($opt_css as $key => $value){
            echo '    <!-- '.$key.' -->'. "\r\n";
            echo '     <link rel="stylesheet" href="'. $value .'">' . "\r\n";
        }
    }
    ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition  <?php echo $ext_data['webcms_style']; ?>  sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url($ext_data['webcms_controllers_folder'].'home'); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b><?php echo $ext_data['webcms_side_title']; ?></b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b><?php echo $ext_data['webcms_side_title']; ?></b><span class="header_subname"> 管理系統</span></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu" style="width: 160px;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url('bower_components/adminlte/dist/img/anno.png'); ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $ext_data['manager_data']['name']; ?></span>
                        </a>
                        <ul class="dropdown-menu" style="width: 50px;right: 0px;">
                            <!-- User image -->
                            <li class="user-header" style="height: 50px;">
                                <a href="<?php echo base_url($ext_data['webcms_controllers_folder'].'login/logout'); ?>" class="btn btn-default btn-flat">登出</a>
                            </li>

                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less 選單功能-->
            <?php
            if(isset($ext_data['sidebar_menu'])){
                echo $ext_data['sidebar_menu'] ;
            }
            ?>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?php
        if(isset($ext_data['page_content'])){
            echo $ext_data['page_content'] ;
        }
        ?>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs"></div>
        <strong>Copyright &copy; <?php echo date("Y"); ?> <span style="color: #666666">LoOuOoK.</strong> All rights reserved.
    </footer>

</div>
<!-- ./wrapper -->
<script>
    <?php $ci =& get_instance();  ?>
    var base_url = '<?php echo base_url(); ?>';
    var cms_path = '<?php echo $ci->config->item('webcms_controllers_folder'); ?>';
    var current_filename = '<?php echo $ci->router->class; ?>';
</script>
<!-- es6 -->
<script src="<?php echo base_url('bower_components/es6/es6-promise.auto.min.js'); ?>"></script>
<script src="<?php echo base_url('bower_components/es6/es6-promise.min.js'); ?>"></script>
<!-- axios -->
<script src="<?php echo base_url('bower_components/axios/axios.min.js'); ?>"></script>
<!-- jQuery 3 -->
<script src="<?php echo base_url("bower_components/jquery/jquery.min.js" );?>" ></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("bower_components/bootstrap/dist/js/bootstrap.min.js" );?>" ></script>
<!-- bootstrap-switch -->
<script src="<?php echo base_url("bower_components/bootstrap-switch/js/bootstrap-switch.min.js" );?>" ></script>
<!-- iCheck -->
<script src="<?php echo base_url("bower_components/adminlte/plugins/iCheck/icheck.min.js" );?>" ></script>
<!-- iCheck -->
<script src="<?php echo base_url('bower_components/toastr/toastr.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("bower_components/jquery-slimscroll/jquery.slimscroll.min.js"); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("bower_components/fastclick/lib/fastclick.js"); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("bower_components/adminlte/dist/js/adminlte.min.js"); ?>"></script>
<!-- jquery_loading_overlay App -->
<script src="<?php echo base_url("bower_components/jquery-loading-overlay/loadingoverlay.min.js"); ?>"></script>
<!-- bootbox -->
<script src="<?php echo base_url("bower_components/bootbox/bootbox.min.js"); ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('bower_components/toastr/toastr.min.js'); ?>"></script>
<!-- parsley -->
<script src="<?php echo base_url('bower_components/parsley/parsley.min.js'); ?>"></script>
<script src="<?php echo base_url('bower_components/parsley/i18n/zh_tw.js'); ?>"></script>


<?php
if(isset( $opt_js)){
    foreach ($opt_js as $key => $value){
        echo '    <!-- '.$key.' -->'. "\r\n";
        echo ' <script src="'.$value.'"></script>' . "\r\n";
    }
}
?>
<script>
    $(function(){
        //open menu
        _menu = '<?php echo $session_menu; ?>';
        $('.m-'+_menu).addClass('menu-open').find('ul').show();
        setTimeout(function(){
            if(_menu != ''){
//                $('.m-'+_menu+' a').trigger('click');
            }
        },800);
    });
</script>
</body>
</html>
