<!-- Content Header (Page header) -->
<section class="content-header">
    <h1> &nbsp; </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('webcms/home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><?php echo $unit_title ; ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"> 新增使用者 </h3>

        </div>
        <div class="box-body">
            <!-- form start -->
            <form  name="frm_01" id="frm_01" method="post">
                <input type="hidden" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
                <input type="hidden" id="uuid" name="uuid" value="<?php echo $uuid ;?>">

                <div class="box-body">
                    <div class="form-group">
                        <label for="所屬群組">所屬群組</label>
                        <select class="form-control" name="group" id="group" required="" data-parsley-error-message="請選擇所屬群組">
                            <option value="" has_roles="" >請選擇...</option>
                            <?php
                            foreach ($query_user_roles_result as $item){
                                echo ' <option value="' .$item->uuid. '" has_roles="' .$item->roles. '" >' .$item->name. '</option>';
                            }
                            ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="名稱">使用者名稱</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="使用者名稱" value="" required="">
                    </div>
                    <div class="form-group">
                        <label for="E-Mail">E-Mail</label>
                        <input type="email" class="form-control" id="email" name="email"  placeholder="E-Mail" value="" required="">
                    </div>
                    <div class="form-group">
                        <label for="密碼">密碼</label>
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="密碼" value="" required="">
                    </div>
                    <div class="form-group">
                        <label for="單元權限">單元權限</label>


                        <table id="data_list" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="ol-lg-3 col-md-3 col-sm-3 col-sm-3">單元</th>
                                <th class="ol-lg-4 col-md-4 col-sm-4 col-sm-4">管理項目</th>
                                <th class="ol-lg-5 col-md-5 col-sm-5 col-sm-5">權限</th>
                            </tr>
                            <tr>
                                <th colspan="3" id="table-msg" style="text-align: center; background-color: #828e7f;color: white;font-weight: 300;letter-spacing: 0.1em">請選擇所屬群組</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //     echo json_encode($side_menu);
                            $check_num = 0;
                            $sm_num = 0;
                            $temp_name = '';
                            foreach($side_menu as $key => $rows_0)
                            {

                                if ($rows_0['status'] == 0) continue;
                                        if (isset($rows_0['sub_menu'])){
                                            foreach ($rows_0['sub_menu'] as  $sub_key => $rows_1)
                                            {
                                                $check_num++;
                                                ?>
                                                <tr id="roles_<?php echo $check_num ; ?>" class="all_roles roles<?php echo $key;?>">
                                                     <?php
                                                    if($rows_0['name'] != $temp_name){
//                                                        echo  ' <td  rowspan="'.sizeof($rows_0['sub_menu']).'"  > ';
                                                        echo  ' <td style="background-color: white;" > ';
                                                        echo $rows_0['name'];
                                                        $temp_name = $rows_0['name'];
                                                        echo '  </td>';
                                                    }else{
                                                        echo  ' <td   style="border-top: none;     background-color: white;"  > ';
                                                        echo '  </td>';
                                                    }

                                                    ?>

                                                <td>
                                                    <?php

                                                    echo  $rows_1['name'];

                                                    ?>

                                                </td>
                                                    <td class="checkbox_td">
                                                <div class="checkbox checkbox-success checkbox-inline">
                                                    <label for="checkbox<?php echo $check_num ; ?>-1">
                                                        <input type="checkbox" name="access_range[]" id="checkbox<?php echo $check_num ; ?>-1" value="<?php echo $rows_1['id'] . '-add'  ; ?>">
                                                        新增
                                                    </label>

                                                </div>

                                                <div class="checkbox checkbox-success checkbox-inline">
                                                    <label for="checkbox<?php echo $check_num ; ?>-2">
                                                        <input type="checkbox" name="access_range[]" id="checkbox<?php echo $check_num ; ?>-2" value="<?php echo $rows_1['id'] . '-mod'  ; ?>">
                                                        修改
                                                    </label>

                                                </div>

                                                <div class="checkbox checkbox-success checkbox-inline">
                                                    <label for="checkbox<?php echo $check_num ; ?>-3">
                                                        <input type="checkbox" name="access_range[]" id="checkbox<?php echo $check_num ; ?>-3" value="<?php echo $rows_1['id'] . '-del'  ; ?>">
                                                        刪除
                                                    </label>

                                                </div>

                                                </td>
                                                </tr>
                                                <?php
                                            }
                                        }else{
                                            $check_num++;
                                            ?>
                                            <tr id="roles_<?php echo $check_num ; ?>" class="all_roles roles<?php echo $key;?>">
                                                <td colspan="2"><?php echo $rows_0['name']; ?></td>
                                                <td class="checkbox_td">
                                            <div class="checkbox checkbox-success checkbox-inline">
                                                <label for="checkbox<?php echo $check_num ; ?>-1">
                                                    <input type="checkbox" name="access_range[]" id="checkbox<?php echo $check_num ; ?>-1" value="<?php echo $rows_0['id'] . '-view'  ; ?>">
                                                    檢視
                                                </label>
                                            </div>

                                            <div class="checkbox checkbox-success checkbox-inline">
                                                <label for="checkbox<?php echo $check_num ; ?>-2">
                                                    <input type="checkbox" name="access_range[]" id="checkbox<?php echo $check_num ; ?>-2" value="<?php echo $rows_0['id'] . '-add'  ; ?>">
                                                    新增
                                                </label>

                                            </div>

                                            <div class="checkbox checkbox-success checkbox-inline">
                                                <label for="checkbox<?php echo $check_num ; ?>-3">
                                                    <input type="checkbox" name="access_range[]" id="checkbox<?php echo $check_num ; ?>-3" value="<?php echo $rows_0['id'] . '-mod'  ; ?>">
                                                    修改
                                                </label>

                                            </div>

                                            <div class="checkbox checkbox-success checkbox-inline">
                                                <label for="checkbox<?php echo $check_num ; ?>-4">
                                                    <input type="checkbox" name="access_range[]" id="checkbox<?php echo $check_num ; ?>-4" value="<?php echo $rows_0['id'] . '-del'  ; ?>">
                                                    刪除
                                                </label>

                                            </div>

                                            </td>
                                            </tr>
                                            <?php
                                        }

                                        ?>


                                <?php


//                                echo  $rows_0['name'];

                                $sm_num++;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class=" col-lg-9">
                            <label for="status">開啟 / 關閉</label>
                            <div class="checkbox">
                                <input type="checkbox" name="item_status" id="item_status" class="margin  item_status"  value="1" checked>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" id="send_form_btn" class="btn btn-primary">送出</button>
                    <small><button type="button" id="goback_btn" class="btn btn-default goback_btn" onclick="window.history.back();">返回列表</button></small>

                </div>
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
<style>
    .checkbox_td{
        background-color: white;padding: 5px;
    }
    .checkbox label {
        line-height: 16px;
        padding-left: 10px;
    }
    .checkbox.checkbox-inline {
        margin-top: 0;
        margin-left: 10px;
    }
    .all_roles{
        display: none;
    }
    input[type=checkbox] {
        transform: scale(1.2);
    }
    .goback_btn{
        float: right;
    }
</style>