<?php
$CI =& get_instance();
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1> &nbsp; </h1>

    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('webcms/home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><?php echo $unit_title ; ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"> <?php echo $unit_title ; ?> </h3>

        </div>
        <div class="box-body">
            <!-- form start -->
            <form role="form"  name="frm_01" id="frm_01" method="post">
                <input type="hidden" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
                <input type="hidden" id="id" name="id" value="<?php echo $output_data['id'] ; ?>">
                <input type="hidden" id="uuid" name="uuid" value="<?php echo $output_data['uuid'] ; ?>">
                <input type="hidden" id="roles_uuid" name="roles_uuid" value="<?php echo $output_data['roles_uuid'] ; ?>">
                <input type="hidden" id="roles" name="roles" value="<?php echo $roles ; ?>">
                <input type="hidden" id="access_range_str" name="access_range_str" value="<?php echo  str_replace(" ","",$output_data['access_range']) ; ?>">


                <div class="box-body">
                    <div class="form-group">
                        <label for="所屬群組">所屬群組</label>
                        <select class="form-control" name="group" id="group" <?php echo  $output_data['account'] == 'administrator' ? 'disabled="disabled"' : '' ; ?> >
                            <option value="" has_roles="" >請選擇...</option>
                            <?php
                            foreach ($query_user_roles_result as $item){
                                if( $item->uuid == $output_data['roles_uuid']){
                                    echo ' <option value="' .$item->uuid. '" has_roles="' .$item->roles. '" selected>' .$item->name. '</option>';
                                }else{
                                    echo ' <option value="' .$item->uuid. '" has_roles="' .$item->roles. '" >' .$item->name. '</option>';

                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="名稱">使用者名稱</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="使用者名稱" value="<?php echo $CI->crypt->decryptdata($output_data['name']) ; ?>"  required="" readonly>
                    </div>
                    <?php
                    if('administrator' == $output_data['email'])
                    {
                        ?>
                    <div class="form-group">
                        <label for="E-Mail">E-Mail</label>
                        <input type="input" class="form-control" id="email" name="email"   value="<?php echo $CI->crypt->decryptdata($output_data['email']) ; ?>"   readonly>
                    </div>
                    <?php
                    }else{
                        ?>
                        <div class="form-group">
                            <label for="E-Mail">E-Mail</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="E-Mail" value="<?php echo $CI->crypt->decryptdata($output_data['email']) ; ?>"  required="" readonly>
                        </div>
                    <?php
                    }
                    $CI =& get_instance();
                    $CI->encryption->initialize(
                        array(
                            'driver' => 'openssl',
                            'cipher' => 'aes-128',
                            'mode' => 'cbc'
                        )
                    );

                    $pass = $CI->encryption->decrypt($output_data['pass']);
                    ?>
                    <div class="form-group">
                        <label for="密碼">密碼</label>
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="密碼" value="<?php echo $CI->crypt->decryptdata($pass) ; ?>" required="">
                    </div>
                    <div class="form-group"  style="<?php echo  $output_data['account'] == 'administrator' ? 'display:none;' : '' ; ?>">
                        <label for="單元權限">單元權限</label>


                        <table id="data_list" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="ol-lg-3 col-md-3 col-sm-3 col-sm-3">單元</th>
                                <th class="ol-lg-4 col-md-4 col-sm-4 col-sm-4">管理項目</th>
                                <th class="ol-lg-5 col-md-5 col-sm-5 col-sm-5">權限</th>
                            </tr>
                            <tr>
                                <th colspan="3" id="table-msg" style="text-align: center; background-color: #828e7f;color: white;font-weight: 300;letter-spacing: 0.1em">請選擇所屬群組</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            //     echo json_encode($side_menu);
                            $check_num = 0;
                            $sm_num = 0;
                            $temp_name = '';
                            $access_range =  explode(",",  str_replace(" ","",$output_data['access_range']));
                              foreach($side_menu as $key => $rows_0)
                            {

                                if ($rows_0['status'] == 0) continue;
                                if (isset($rows_0['sub_menu'])){
                                    foreach ($rows_0['sub_menu'] as  $sub_key => $rows_1)
                                    {
                                        $check_num++;
                                        ?>
                                        <tr id="roles_<?php echo $check_num ; ?>" class="all_roles roles<?php echo $key;?>">
                                            <?php
                                            if($rows_0['name'] != $temp_name){
//                                                        echo  ' <td  rowspan="'.sizeof($rows_0['sub_menu']).'"  > ';
                                                echo  ' <td style="background-color: white;" > ';
                                                echo $rows_0['name'];
                                                $temp_name = $rows_0['name'];
                                                echo '  </td>';
                                            }else{
                                                echo  ' <td   style="border-top: none;     background-color: white;"  > ';
                                                echo '  </td>';
                                            }

                                            ?>

                                            <td>
                                                <?php

                                                echo  $rows_1['name'];

                                                ?>

                                            </td>
                                            <td class="checkbox_td">

                                                <div class="checkbox checkbox-success checkbox-inline">
                                                    <input type="checkbox" id="checkbox<?php echo $check_num ; ?>-1" name="access_range[]" value="<?php echo $rows_1['id'] . '-add'  ; ?>" class="styled" <?php echo (in_array( $rows_1['id'] . '-add',$access_range) ? 'checked': '') ?> >
                                                    <label for="checkbox<?php echo $check_num ; ?>-1">
                                                        新增
                                                    </label>

                                                </div>

                                                <div class="checkbox checkbox-success checkbox-inline">
                                                    <input type="checkbox" id="checkbox<?php echo $check_num ; ?>-2" name="access_range[]" value="<?php echo $rows_1['id'] . '-mod'  ; ?>" class="styled" <?php echo (in_array( $rows_1['id'] . '-mod',$access_range) ? 'checked': '') ?> >
                                                    <label for="checkbox<?php echo $check_num ; ?>-2">
                                                        修改
                                                    </label>

                                                </div>

                                                <div class="checkbox checkbox-success checkbox-inline">
                                                    <input type="checkbox" id="checkbox<?php echo $check_num ; ?>-3" name="access_range[]" value="<?php echo $rows_1['id'] . '-del'  ; ?>" class="styled" <?php echo (in_array( $rows_1['id'] . '-del',$access_range) ? 'checked': '') ?> >
                                                    <label for="checkbox<?php echo $check_num ; ?>-3">
                                                        刪除
                                                    </label>

                                                </div>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }else{
                                    $check_num++;
                                    ?>
                                    <tr id="roles_<?php echo $check_num ; ?>" class="all_roles roles<?php echo $key;?>">
                                        <td colspan="2"><?php echo $rows_0['name']; ?></td>
                                        <td class="checkbox_td">

                                            <div class="checkbox checkbox-success checkbox-inline">
                                                <input type="checkbox" id="checkbox<?php echo $check_num ; ?>-1" name="access_range[]" value="<?php echo $rows_0['id']  . '-add'  ; ?>" class="styled" <?php echo (in_array( $rows_0['id'] . '-add',$access_range) ? 'checked': '') ?> >
                                                <label for="checkbox<?php echo $check_num ; ?>-1">
                                                    新增
                                                </label>

                                            </div>

                                            <div class="checkbox checkbox-success checkbox-inline">
                                                <input type="checkbox" id="checkbox<?php echo $check_num ; ?>-2" name="access_range[]" value="<?php echo $rows_0['id'] . '-mod'  ; ?>" class="styled" <?php echo (in_array( $rows_0['id'] . '-mod',$access_range) ? 'checked': '') ?> >
                                                <label for="checkbox<?php echo $check_num ; ?>-2">
                                                    修改
                                                </label>

                                            </div>

                                            <div class="checkbox checkbox-success checkbox-inline">
                                                <input type="checkbox" id="checkbox<?php echo $check_num ; ?>-3" name="access_range[]" value="<?php echo $rows_0['id'] . '-del'  ; ?>"  class="styled" <?php echo (in_array( $rows_0['id'] . '-del',$access_range) ? 'checked': '') ?> >
                                                <label for="checkbox<?php echo $check_num ; ?>-3">
                                                    刪除
                                                </label>

                                            </div>

                                        </td>
                                    </tr>
                                    <?php
                                }

                                ?>


                                <?php


//                                echo  $rows_0['name'];

                                $sm_num++;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <?php
                    if('administrator' != $output_data['email'])
                    {
                        ?>
                        <div class="row">
                            <div class=" col-lg-9">
                                <label for="status">開啟 / 關閉</label>
                                <div class="checkbox">
                                    <input type="checkbox" name="item_status" id="item_status"
                                           class="margin  item_status"
                                           value="1" <?php echo ($output_data['item_status'] == 1) ? 'checked' : ''; ?>>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <!-- /.box-body -->


            </form>
        </div>
        <!-- /.box-body -->
    </div>

    <!--     建立人員及修改人員紀錄 -->
    <div class="box member_records_box collapsed-box">
        <div class="box-header with-border box-tools">
            <button type="button" class="btn btn-box-tool"   data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse" style="width:100%; font-size: 18px; text-align: left; padding: 7px 10px; margin-top: 0;">
                <i class="fa fa-plus"></i>   建立人員及修改人員時間紀錄 </button>
        </div>
        <div class="box-body">
            <div class="form-group">
                <div style="background-color:white; margin: 10px;padding: 10px;">
                    <span style="color: #555555;font-weight: 600;">最近修改人員與時間：</span>
                    <?php
                    foreach ($query_result_update_data as $item){
                        echo $item->name . '&nbsp;&nbsp;';
                        echo date('Y月m月d日  H:i',strtotime( $output_data['update_datetime'] ));
                    }
                    if(sizeof($query_result_update_data) == 0){
                        echo '無';
                    }

                    ?>

                </div>

                <div style="background-color:white; margin: 10px;padding: 10px;">
                    <span style="color: #555555;font-weight: 600;">建立人員與時間：</span>
                    <?php
                    foreach ($query_result_create_data as $item){
                        echo $item->name . '&nbsp;&nbsp;';
                        echo date('Y月m月d日  H:i',strtotime( $output_data['create_datetime'] ));
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.建立人員及修改人員紀錄 -->

    <!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" id="send_form_btn" class="btn btn-primary">送出</button>
        <small><button type="button" id="goback_btn" class="btn btn-default goback_btn" onclick="window.history.back();">返回列表</button></small>

    </div>
    <!-- /.box-footer-->


</section>
<!-- /.content -->
<style>
    .checkbox_td{
        background-color: white;padding: 5px;
    }
    .checkbox label {
        line-height: 16px;
        padding-left: 10px;
    }
    .checkbox.checkbox-inline {
        margin-top: 0;
        margin-left: 10px;
    }
    .all_roles{
        display: none;
    }
    input[type=checkbox] {
        transform: scale(1.2);
    }
    .goback_btn{
        float: right;
    }
</style>