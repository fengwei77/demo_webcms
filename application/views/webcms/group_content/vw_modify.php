<!-- Content Header (Page header) -->
<section class="content-header">
    <h1> &nbsp; </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('webcms/home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><?php echo $unit_title ; ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">

            <h3 class="box-title"> 修改群組 </h3>

        </div>
        <div class="box-body">
            <!-- form start -->
            <form  name="frm_01" id="frm_01" method="post" data-parsley-validate="">
                <input type="hidden" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
                <input type="hidden" id="id" name="id" value="<?php echo $output_data['id'] ; ?>">
                <input type="hidden" id="uuid" name="uuid" value="<?php echo $output_data['uuid'] ; ?>">

                <div class="box-body">
                    <div class="form-group">
                        <label for="群組名稱">群組名稱</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="請輸入群組名稱" value="<?php echo $output_data['name'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label for="管理單元">管理單元</label>
                        <?php
                        //     echo json_encode($side_menu);
                        $sm_num = 0;
                        $has_roles =explode(",",  $output_data['roles']);
                        foreach($side_menu as $key => $rows_0)
                        {
                            if ($rows_0['status'] == 0) continue;
                            echo '  <div class="checkbox">';
                            echo ' <label for="checkbox'. $sm_num .'">';
                            echo ' <input id="checkbox'. $sm_num .'" class="styled" type="checkbox" value="'.$key.'" name="roles" '. (in_array($key,$has_roles) ? 'checked': '') .'>';
                            echo  $rows_0['name'];
                            echo ' </label>';
                            echo '  </div>';

                            $sm_num++;
                        }
                        ?>
                    </div>
                    <div class="row">



                        <div class=" col-lg-9">
                            <label for="status">開啟 / 關閉</label>
                            <div class="checkbox">
                                <input type="checkbox" name="item_status" id="item_status" class="margin  item_status" value="1" <?php echo  ($output_data['item_status'] == 1) ? 'checked': '' ; ?>>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->

            </form>
        </div>
    </div>


        <!--     建立人員及修改人員紀錄 -->
        <div class="box member_records_box collapsed-box">
            <div class="box-header with-border box-tools">
                <button type="button" class="btn btn-box-tool"   data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse" style="width:100%; font-size: 18px; text-align: left; padding: 7px 10px; margin-top: 0;">
                    <i class="fa fa-plus"></i>   建立人員及修改人員時間紀錄 </button>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div style="background-color:white; margin: 10px;padding: 10px;">
                        <span style="color: #555555;font-weight: 600;">最近修改人員與時間：</span>
                        <?php
                             foreach ($query_result_update_data as $item){
                                echo $item->name . '&nbsp;&nbsp;';
                             }

                        echo date('Y月m月d日  H:i',strtotime( $output_data['update_datetime'] ));
                        ?>

                    </div>

                    <div style="background-color:white; margin: 10px;padding: 10px;">
                        <span style="color: #555555;font-weight: 600;">建立人員與時間：</span>
                        <?php
                        foreach ($query_result_create_data as $item){
                            echo $item->name . '&nbsp;&nbsp;';
                            echo date('Y月m月d日  H:i',strtotime( $output_data['create_datetime'] ));
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" id="send_form_btn" class="btn btn-primary">送出</button>
            <small><button type="button" id="goback_btn" class="btn btn-default goback_btn" onclick="window.history.back();">返回列表</button></small>

        </div>
        <!-- /.box-footer-->

    <!-- /.box -->

</section>
<!-- /.content -->
<style>
    .checkbox{
        background-color: white;padding: 5px;
    }

    .checkbox label {
        line-height: 16px;
    }
    input[type=checkbox] {
        transform: scale(1.2);
    }
    .goback_btn{
        float: right;
    }
</style>