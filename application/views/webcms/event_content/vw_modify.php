<!-- Content Header (Page header) -->
<section class="content-header">
    <h1> &nbsp; </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('webcms/home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><?php echo $unit_title ; ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- form start -->
    <form  name="frm_01" id="frm_01" method="post" data-parsley-validate="">
        <input type="hidden" id="csrf" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
        <input type="hidden" id="id" name="id" value="<?php echo $output_data['id'] ; ?>">
        <input type="hidden" id="uuid" name="uuid" value="<?php echo $output_data['uuid'] ; ?>">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $unit_title ; ?> </h3>

            </div>
            <div class="box-body">
                <!-- form start -->
                <input type="hidden" id="guid" name="guid" value="">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="title"> 完整標題   </label>
                            <input type="text" class="form-control" name="title"  id="title" placeholder=" 完整標題  " value="<?php echo $output_data['title'] ; ?>"  required="" maxlength="255">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="short_title"> 列表標題   </label>
                            <input type="text" class="form-control" name="short_title"  id="short_title" placeholder=" 短標題  " value="<?php echo $output_data['short_title'] ; ?>"  required="" maxlength="255">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="content_1"> 內容   </label>
                            <?php

                            $content_1 =  html_entity_decode($output_data['content_1'], ENT_QUOTES,"UTF-8");

                            echo '<textarea class="form-control content_1" name="content_1"  id="content_1" placeholder=" 輸入內容  ">'.$content_1.'</textarea>';
                            ?>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /.box -->

        <div class="box">


            <div class="box-header with-border box-tools">
                <h3 class="box-title"> 上傳圖片   </h3>
            </div>
            <div class="box-body">
                <?php
                $pic_ratio_show_arr = array(
                    '列表圖片尺寸380x220',
                    '內容主圖片尺寸900x520',
                ) ;
                $pic_ratio_arr = array(
                    '380,220',
                    '900,520',
                ) ;
                $files_arr = json_decode( $output_data['files'],TRUE);
                $files_path = array_fill(0, sizeof($pic_ratio_arr) , '');
                if(sizeof($files_arr)>0)
                {
                    foreach ($files_arr as $key => $value)
                    {
                        if (isset($value[$key]) > 0 && $value != NULL)
                        {
                            if(isset( $value[$key]['file_path']))
                            {
                                $files_path [$key] = $value[$key]['file_path'];
                            }else  {
                                $files_path [$key] = '';
                            }
                        } else
                        {
                            $files_path [$key] = '';
                        }
                    }
                }
                for($i = 0;$i < sizeof($pic_ratio_arr) ;$i++  ){
                    $upload_pic_num = $i ;
                    $upload_pic_info = ''.$pic_ratio_show_arr[$i] .' , 上傳檔案限制為2MB</br>格式建議為jpg , png 為主。';
                    ?>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div id="upload_pic_<?php echo $upload_pic_num; ?>" class="upload_pic_box">
                            <div class="form-group upload_pic_inner_box">
                                <input type="hidden" class="form-control" name="upload_desc_<?php echo $upload_pic_num; ?>" file_id="<?php echo $upload_pic_num; ?>"  id="upload_desc_<?php echo $upload_pic_num; ?>" placeholder="圖片說明" value=""  >

                                <div class="checkbox" style="display: none; margin-left: 25px;transform: scale(1.2);padding-bottom: 5px;">
                                    <input type="checkbox" name="show_upload_pic_<?php echo $upload_pic_num; ?>" file_id="<?php echo $upload_pic_num; ?>"  id="show_upload_pic_<?php echo $upload_pic_num; ?>" class="margin "  checked>
                                </div>
                                <label for="show_upload_pic_<?php echo $upload_pic_num; ?>" style="margin-right: 20px;"><?php echo $upload_pic_info; ?> </label>
                                <a  href="<?php echo   $files_path[$i] != "" ?   base_url ( $files_path[$i]) : '' ; ?>" class="preview_fancybox" id="preview_fancybox_<?php echo $upload_pic_num; ?>">
                                    <img id="preview_upload_pic_<?php echo $upload_pic_num; ?>"   src="<?php echo $files_path[$i] != "" ?    base_url ($files_path[$i]) : ''  ; ?>"  height="30px">
                                </a>
                            </div>

                            <div class="row" style="margin-top: 5px;">
                                <div class="col-lg-3 col-md-3 col-xs-4" id="upload_pic_btn_<?php echo $upload_pic_num; ?>">
                                    <input type="file" class="form-control filestyle upload_files" file_id="<?php echo $upload_pic_num; ?>" name="upload_files_<?php echo $upload_pic_num; ?>"  id="upload_files_<?php echo $upload_pic_num; ?>" data-text="開啟檔案" data-input="false" data-badge="false"  data-buttonBefore="true" data-placeholder="尚未選擇檔案">
                                </div>

                                <div class="col-lg-3 col-md-4 col-xs-4">
                                    <button type="button" class="btn btn-success  file_cropper" file_id="<?php echo $upload_pic_num; ?>" id="file_cropper_<?php echo $upload_pic_num; ?>" style="display: none;" onclick="cropper_fire( '<?php echo $upload_pic_num; ?>', '<?php echo $pic_ratio_arr[$i]; ?>');"  >裁切圖片</button>
                                </div>

                                <div class="col-lg-2 col-md-3 col-xs-3">
                                    <button type="button" class="btn btn-danger del_upload_pic" file_id="<?php echo $upload_pic_num; ?>"  id="del_upload_pic_<?php echo $upload_pic_num; ?>"  style="display: none;">刪除</button>
                                </div>
                            </div>
                        </div>
                        <!-- /.upload_pic_<?php echo $upload_pic_num; ?> -->
                    </div>
                    <?php
                }
                ?>
                <!-- /.2張圖片 -->
            </div>
        </div>

        <div class="box all_datetime_box">
            <div class="box-body">

                <div class="form-group">
                    <div style="display: inline-block">

                        <label for="order_num"> 上架時間</label>
                        <div class="form-group">
                            <div class='input-group date' id='bs_start_date' style="width:300px; ">
                                <input type='text' class="form-control" name="start_date" id="start_date" value="<?php echo $output_data['start_date']; ?>" />
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>
                    </div>

                    <div  style="display: inline-block">
                        <label for="order_num"> 下架時間</label>

                        <div class="form-group">
                            <div class='input-group date' id='bs_expire_date' style="width:300px;">
                                <input type='text' class="form-control" name="expire_date" id="expire_date"  value="<?php echo $output_data['expire_date']; ?>" />
                                <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="  col-lg-3" style="display:  ;">
                    <label for="sort_number">排序 (順序數字大至小向下排序)</label>
                    <input type="number" min="0" max="99999" class="form-control sort_number" id="sort_number" name="sort_number" placeholder="排序" value="<?php echo $output_data['sort_number'] ; ?>" required="" style="width: 100px;" maxlength="5"  onchange=" if($(this).val() != 0){ $(this).val(($(this).val().replace(/^0+/, '')))}">
                </div>

                <div class=" col-lg-3">
                    <label for="item_status">開啟 / 關閉</label>
                    <div class="checkbox">
                        <input type="checkbox" name="item_status" id="item_status" class="margin  item_status" value="1" <?php echo  ($output_data['item_status'] == 1) ? 'checked': '' ; ?>>
                    </div>
                </div>
            </div>
        </div>


        <div class="box-footer">
            <button type="submit" id="send_form_btn" class="btn btn-primary">送出</button>
            <small><button type="button" id="goback_btn" class="btn btn-default goback_btn" onclick="window.history.back();">返回列表</button></small>
        </div>
        <!-- /.box-body -->


    </form>
</section>
<!-- /.content -->

<div id="cropper_box" style="width:75%;height:80%; display: none;">
    <div id="contain_box" style="width:100%;height: 88%; ">
        <img id="crop_img"  />
        <div class="col-lg-6 col-md-6" style="margin-bottom: 10px;margin-top: 15px;">
            <button type="button" class="cancel_crop btn btn-warning btn-block" onclick=" cancel_crop(0);" >取消裁切</button>
        </div>
        <div class="col-lg-6 col-md-6" style="margin-bottom: 10px;margin-top: 15px;">
            <button type="button" class="basic-result btn btn-success btn-block">裁切</button>
        </div>

    </div>
</div>

<a href="#cropper_box" class="cropper_open" style="display: none;">cropper_open</a>
<!-- /.cropper -->


<script>

    var files_array = new  Array(<?php echo sizeof($pic_ratio_arr) ;?>);

    var files_array_temp = new  Array(<?php echo sizeof($pic_ratio_arr) ;?>);
    <?php
    $files_arr = json_decode( $output_data['files'],TRUE);
    $files_path = array(sizeof($pic_ratio_arr));
    foreach ( $files_arr as $key => $value){
        if(isset($value[$key]) > 0 && $value != null){
            if(isset( $value[$key]['file_path']))
            {
                $files_path [$key] = $value[$key]['file_path'];
            }else  {
                $files_path [$key] = '';
            }
        }else{
            $files_path [$key] = '';
        }
    }
    if(sizeof($files_arr)>0){
        echo  'files_array_temp = '. json_encode($files_path) .';';
    }
    ?>
    for(var i=0;i<files_array.length;i++){
        var obj = {};
        var obj_content = {};
        obj_content[ 'file_path'] = files_array_temp[i];
        obj[i] = obj_content;
        files_array[(i)] = obj;
    }

    var start_date = '<?php echo $output_data['start_date']; ?>';
    var expire_date = '<?php echo $output_data['expire_date']; ?>';
</script>
<style>

    .checkbox label {
        line-height: 16px;
        padding-left: 10px;
    }
    .checkbox.checkbox-inline {
        margin-top: 0;
        margin-left: 10px;
    }
    .all_roles{
        display: none;
    }
    input[type=checkbox] {
        transform: scale(1.2);
    }

    .date_box{
        display: inline-block;
        width:100px;
    }
    .goback_btn{
        float: right;
    }

</style>