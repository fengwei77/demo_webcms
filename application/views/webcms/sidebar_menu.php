<?php
//取使用者權限

$CI =& get_instance();
//先判斷有沒有資格
$prefix_page_name = 'prefix_page_name_def';
$search_filter = array('_sub', '_setting', '_content', '_qa');
$class_name = str_replace($search_filter, '', $CI->router->fetch_class());
$this_menu_id = '';
foreach ($side_menu as $key => $rows_0)
{
    if (isset($rows_0['pagename']))
    {


        if ($rows_0['pagename'] == $class_name)
        {

            $this_menu_id = $key;
            break;
        }
    }
    if ($rows_0['status'] == 0)
    {
        continue;
    }
    if (isset($rows_0['sub_menu']))
    {
        foreach ($rows_0['sub_menu'] as $rows_1)
        {
            if ($rows_1['pagename'] == $class_name)
            {
                $this_menu_id = $key;
            }


        }
    }
}

$CI->load->model('basic_model');
if(isset( $_SESSION['manager_data'])){
    $uuid =   $_SESSION['manager_data']['uuid'];

    $CI->basic_model->initialize( "tb_users" );
    // get_all($fields = '', $where = array(), $table = '', $limit = '' , $start = '0', $order_by = '', $group_by = '')
    $where_arr = array(
        'uuid' => $uuid,
        'has_deleted' => 0
    );

    $output_data = array();
    $query_result= $CI->basic_model->get_where($where_arr);
    foreach($query_result  as  $row){
        //取資料
        $output_data  = array(
            'id' => $row->id,
            'uuid' => $row->uuid,
            'roles_id' => $row->roles_id,
            'roles_uuid' => $row->roles_uuid,
            'name' => $row->name,
            'account' => $row->account,
            'pass' => $row->pass,
            'email' => $row->email,
            'level' => $row->level,
            'access_range' => $row->access_range,
            'item_status' =>  $row->item_status,
            'create_user_uuid' => $row->create_user_uuid,
            'modify_user_uuid' => $row->modify_user_uuid,
            'update_datetime' => $row->update_datetime,
            'create_datetime' => $row->create_datetime
        );
//        echo $output_data['access_range'];
        //取群組選單
        $CI->basic_model->initialize( "tb_user_roles" );
        $where_arr = array(
        );
        $query_user_roles_result= $CI->basic_model->get_where($where_arr);
        $roles ='';
        $roles_save ='';
        foreach (  $query_user_roles_result as $item){
            if( $item->uuid ==  $output_data['roles_uuid']){
                $roles = $item->roles;
                $roles_save = $item->roles;
            }
        }
        if($roles == 'all'){
            $roles = '';
            for($i=1;$i<sizeof($side_menu);$i++){
                if($i > 1){
                    $roles .=',';
                }
                $roles .= $i;
            }
        }
        if(in_array($this_menu_id,explode(",",$roles))){
            //有權限
        }else{
            if($class_name!= 'home'){
                if($roles_save == 'all'){

                }else{
                    redirect(base_url($CI->config->item('webcms_controllers_folder').'home'), 'location', 301);

                }

            }
        }

    }
}else{
    redirect(base_url($CI->config->item('webcms_controllers_folder').'/login'), 'location', 301);
}
?>

<ul class="sidebar-menu" data-widget="tree">
    <li class="header">功能選單</li>


<!-- 管理員  -->
    <?php
    //     echo json_encode($side_menu);
//    print_r($side_menu);

//    echo $roles;
    $roles_arr = explode(",", $roles);
    $show_menu = array();
for($i = 0 ; $i < sizeof($roles_arr) ;$i++){
    array_push( $show_menu,$side_menu[$roles_arr[$i]]);
}
    foreach ($show_menu as $rows_0)
    {
        if ($rows_0['status'] == 0) continue;

//        echo ' <!-- ' . $rows_0['name'] . '  -->';

        if (isset($rows_0['sub_menu'])){
            echo '<li class="treeview  ' . $rows_0['tag'] . ' " >';
            echo '<a href="#">';
            echo '  <i class="fa fa-th-large"></i> <span>' . $rows_0['name'] . '</span>';
            echo '  <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';
            echo ' </a>';

            echo '<ul class="treeview-menu">';
            foreach ($rows_0['sub_menu'] as $rows_1)
            {
                $search_filter  = array('_sub', '_setting','_content','_qa');

                if(isset($_SESSION['ss_pagename'])? (str_replace( $search_filter,'',$_SESSION['ss_pagename']) == $rows_1['pagename']) :FALSE){
                    echo '<li class="active"><a href="'. base_url($webcms_controllers_folder . $rows_1['pagename'] ) . '"><i class="fa fa-circle-o"></i> '. $rows_1['name'] .'</a></li>';

                }else{
                    echo '<li class=""><a href="'. base_url($webcms_controllers_folder . $rows_1['pagename'] ) . '"><i class="fa fa-circle-o"></i> '. $rows_1['name'] .'</a></li>';

                }

            }
            echo '</ul>';
        }else{
            echo '<li class="' . $rows_0['tag'] . ' " >';
            echo '<a href="'. base_url($webcms_controllers_folder . $rows_0['pagename'] ) . '">';
            echo '  <i class="fa fa-square-o"></i> <span>' . $rows_0['name'] . '</span>';
            echo ' </a>';
        }
        echo '</li>';
    }
    ?>

</ul>