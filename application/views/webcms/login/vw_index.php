<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $webcms_title .' | 首頁'?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/bootstrap/dist/css/bootstrap.min.css" );?>" >
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/font-awesome/css/font-awesome.min.css" );?>" >
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/Ionicons/css/ionicons.min.css" );?>" >
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/adminlte/dist/css/AdminLTE.min.css" );?>" >
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    <link rel="stylesheet" href="<?php echo base_url('bower_components/adminlte/dist/css/skins/_all-skins.css'); ?>">
    <!-- toastr -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/toastr/toastr.min.css" );?>" >
    <!-- parsley -->
    <link rel="stylesheet" href="<?php echo base_url('bower_components/parsley/parsley.css'); ?>">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
    .login-page, .register-page {
        background: ##2e2806;
        background-image: url("<?php echo base_url('bower_components/adminlte/dist/img/zenbg-1.png'); ?>"), url("<?php echo base_url('bower_components/adminlte/dist/img/zenbg-2.png'); ?>");
        background-repeat: repeat-y, repeat;
        height: 80vh;
    }
    .login-logo a, .register-logo a {
        color: #ffffff;
    }
</style>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b class="font_title"><?php echo $webcms_title  ?></b> <span class="font_title_ext">管理系統</span> </a>
    </div>
    <!-- /.login-logo -->
    <div class="bs-callout bs-callout-warning hidden">
        <h4>Oh snap!</h4>
        <p>This form seems to be invalid :(</p>
    </div>

    <div class="bs-callout bs-callout-info hidden">
        <h4>Yay!</h4>
        <p>Everything seems to be ok :)</p>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">請輸入帳號密碼</p>

        <form  name="frm_01" id="frm_01" method="post">
            <div class="invalid-form-error-message"></div>
            <input type="hidden" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
            <div class="form-group has-feedback">
                <input type="text" id="account" name="account"  class="form-control" placeholder="請輸入帳號" value=""  required="" data-parsley-error-message="">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="password" name="password"  class="form-control" placeholder="請輸入密碼" value=""  required="" data-parsley-error-message="">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    <button type="button" class="btn btn-primary btn-block btn-flat" id="login_btn" >登入</button>
                </div>
                <!-- /.col -->
            </div>  </form>



    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<!-- es6 -->
<script src="<?php echo base_url('bower_components/es6/es6-promise.auto.min.js'); ?>"></script>
<script src="<?php echo base_url('bower_components/es6/es6-promise.min.js'); ?>"></script>
<!-- axios -->
<script src="<?php echo base_url('bower_components/axios/axios.min.js'); ?>"></script>
<!-- jQuery 3 -->
<script src="<?php echo base_url("bower_components/jquery/jquery.min.js" );?>" ></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("bower_components/bootstrap/dist/js/bootstrap.min.js" );?>" ></script>
<!-- iCheck -->
<script src="<?php echo base_url("bower_components/adminlte/plugins/iCheck/icheck.min.js" );?>" ></script>
<!-- iCheck -->
<script src="<?php echo base_url('bower_components/toastr/toastr.min.js'); ?>"></script>
<!-- parsley -->
<script src="<?php echo base_url('bower_components/parsley/parsley.min.js'); ?>"></script>
<script src="<?php echo base_url('bower_components/parsley/i18n/zh_tw.js'); ?>"></script>
<!-- all_site_js -->
<script src="<?php echo base_url('assets/webcms/js/all_site.js'); ?>"></script>

<!-- customs -->
<script>
    <?php
    $ci =& get_instance();
    ?>
    var base_url = '<?php echo base_url(); ?>';
    var cms_path = '<?php echo $ci->config->item('webcms_controllers_folder'); ?>';
    var current_filename = '<?php echo $ci->router->class; ?>';
    var full_base_url = base_url + cms_path + current_filename + '/';
</script>
<script src="<?php echo base_url('assets/webcms/js/login.js'); ?>"></script>
<script>
    $(function () {


    });
</script>
</body>
</html>
