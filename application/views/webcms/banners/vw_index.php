<?php
//取使用者權限
$CI =& get_instance();
$CI->load->model('basic_model');
$CI->load->model('admin_model');

$access_range = array();
$func_id = '';
if(isset( $_SESSION['manager_data'])){
    $uuid =   $_SESSION['manager_data']['uuid'];
    $access_range = $CI->admin_model->get_access_range_id($uuid);

    $prefix_page_name = 'prefix_page_name_def';
    $search_filter  = array('_sub', '_setting','_content','_qa');

    if( isset($_SESSION['ss_pagename']) ){
        $prefix_page_name = str_replace( $search_filter,'',$_SESSION['ss_pagename']);
    }
    foreach ($side_menu as $rows_0)
    {
        if(isset($rows_0['pagename'])){


            if(  $rows_0['pagename']  ==  $prefix_page_name){

                $func_id = $rows_0['id'];
                break;
            }

        }
        if ($rows_0['status'] == 0) continue;
        if (isset($rows_0['sub_menu'])){
            foreach ($rows_0['sub_menu'] as $rows_1)
            {
                if(  $rows_1['pagename']  ==  $prefix_page_name){
                    $func_id = $rows_1['id'];
                }
            }
        }
    }

}else{
    redirect(base_url($CI->config->item('webcms_controllers_folder').'/login'), 'location', 301);
}

$user_add = str_replace(" ","", $func_id.'-add');
$user_mod = str_replace(" ","", $func_id.'-mod');
$user_del = str_replace(" ","", $func_id.'-del');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('webcms/home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><?php echo $unit_title ; ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">


    <form  name="frm_search" id="frm_search" method="get">
        <input type="hidden" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
        <div class="box">
            <div class="box-header with-border">

                <h4 class="box-title">搜尋操作</h4>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <div class="box-body">
                    <div class="col-lg-3 col-md-3 col-xs-3">
                        <select class="form-control search_status" id="search_status" name="search_status" >
                            <option value="" <?php echo $search_status == "" ? "selected" : "" ; ?>>啟用狀態</option>
                            <option value="1" <?php echo $search_status == "1" ? "selected" : "" ; ?>>啟用</option>
                            <option value="0" <?php echo $search_status == "0" ? "selected" : "" ; ?>>關閉</option>
                        </select>
                    </div>
                    <div class="col-xs-1">
                        <button type="button" id="search_btn" class="btn btn-default search_btn">搜尋</button>

                    </div>
                    <div class="col-xs-1">
                        <button type="button" id="reset_btn" class="btn btn-default reset_btn">重置</button>

                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="box">
        <div class="box-body">
            <!-- /.box-header -->
            <div class="box-header">
                <small>
                    <?php
                    // echo $_SESSION['ss_page_size'];
                    // echo $prefix_page_name;
                    $page_size_arr = array(20,50,200);
                    $page_size = 20;
                    if(isset($_SESSION['ss_page_size_name']) == $prefix_page_name){
                        $page_size = $_SESSION['ss_page_size'];
                    }
                    ?>
                    <span class="col-lg-3 col-md-3 col-xs-3">
                    <select class="form-control"  id="page_size_select" name="page_size_select" style="width: 120px;" >
                        <?php
                        foreach ($page_size_arr as $row){
                            $show_row_num = $row . '筆';
                            if($page_size == $row ){
                                echo '    <option value="'.$row.'" selected>顯示'.$show_row_num.'</option>';
                            }else{
                                echo '    <option value="'.$row.'">顯示'.$show_row_num.'</option>';

                            }
                        }

                        ?>
                    </select>
                        </span>
                    <span class="col-lg-9 col-md-9 col-xs-9">
                <?php
                if(in_array($user_add , $access_range)){
                    echo '  <button type="button" id="add_btn" class="btn btn-default add_btn" >新增項目</button>';
                }
                ?>
                    </span>
                </small>
            </div>
            <table id="data_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-lg-1  col-sm-1 col-xs-1">大圖片</th>
                    <th class="col-lg-1  col-sm-1 col-xs-1">小圖片</th>
                    <th class="col-lg-1  col-sm-2 col-xs-2">素材名稱</th>
                    <th class="col-lg-2  col-sm-2 col-xs-2">連結</th>
                    <!--                        <th class="col-lg-1  col-sm-1 col-xs-1">點閱數</th>-->
                    <th class="col-lg-2  col-sm-1 col-xs-1 do_sort"  id="create_datetime"  sort="">建立時間 <i class="fa fa-sort" aria-hidden="true"></i></th>
                    <th class="col-lg-1  col-sm-1 col-xs-1 do_sort"  id="sort_number"  sort="">排序 <i class="fa fa-sort" aria-hidden="true"></i></th>
                    <th class="col-lg-2  col-sm-2 col-xs-2">操作</th>
                </tr>
                </thead>
                <tbody id="data_msg" >
                <tr>
                    <td colspan="8" align="center">Loading... </td>
                </tr>
                </tbody>
                <tbody id="data_body" style="display: none;">
                <?php
                for($i = 0; $i <sizeof($query_all); $i++){
                    ?>
                    <tr class="row_<?php echo  $query_all[$i]['uuid']; ?>">
                        <!--    <td > --><?php //echo $page_num + $i + 1?><!-- </td>-->
                        <td>
                            <?php
                            if($query_all[$i]['file_1'] != ''){
                                ?>
                                <a href="<?php echo base_url(  $query_all[$i]['file_1'] ); ?>" class="preview_fancybox" id="preview_fancybox_1_<?php echo $query_all[$i]['uuid']; ?>">
                                    <img id="preview_upload_pic_<?php echo $query_all[$i]['uuid']; ?>" src="<?php echo base_url(  $query_all[$i]['file_1'] ); ?>" height="30px">
                                </a>
                            <?php
                            }else{
                                ?>
                                未上傳
                                <?php
                            }
                            ?>

                        </td>
                        <td>
                            <?php
                            if($query_all[$i]['file_2'] != ''){
                                ?>
                                <a href="<?php echo base_url(  $query_all[$i]['file_2'] ); ?>" class="preview_fancybox" id="preview_fancybox_2_<?php echo $query_all[$i]['uuid']; ?>">
                                    <img id="preview_upload_pic_<?php echo $query_all[$i]['uuid']; ?>" src="<?php echo base_url(  $query_all[$i]['file_2'] ); ?>" height="30px">
                                </a>
                                <?php
                            }else{
                                ?>
                                未上傳
                                <?php
                            }
                            ?>
                        </td>
                        <td><?php echo $query_all[$i]['name'] ; ?> </td>

                        <td><?php echo $query_all[$i]['link'] ; ?> </td>
                        <td><?php echo $query_all[$i]['create_datetime'] ; ?> </td>
                        <?php
                        if(in_array($user_mod , $access_range)){
                            echo ' <td>';
                            echo '<a href="#" id="order_num" class="order_num" data-type="number"  data-min="0" data-max="99999" data-pk="1" uuid="'. $query_all[$i]['uuid'] .'">'. $query_all[$i]['sort_number'].'</a>';
                            echo '</td>';
                        }else{
                            echo ' <td>';
                            echo $query_all[$i]['sort_number'];
                            echo '</td>';
                        }
                        ?>

                        <td class="control_group">

                            <div class="btn-group">
                                <?php
                                if(in_array($user_mod , $access_range)){
                                    echo '<input type="checkbox" name="item_status" id="item_status-'.$query_all[$i]['id'].'" class="margin  item_status" uuid="'.$query_all[$i]['uuid'].'" '.  ($query_all[$i]['item_status'] == 1 ? 'checked':'') .'>';
                                }else{
                                    echo '<span class="bg-gray disabled color-palette" style="padding: 5px;width: 68px; margin-right: 5px;">';
                                    echo  $query_all[$i]['item_status'] == 1 ? '啟用中 ':'關閉中';
                                    echo '</span>';
                                }
                                if(in_array($user_mod , $access_range)){
                                    echo '<button type="button" class="btn-xs btn-warning modify_btn" uuid="'.$query_all[$i]['uuid'].'">修改</button>';
                                }
                                if(in_array($user_del , $access_range)){
                                    echo '<button type="button" class="btn-xs btn-danger delete_btn" uuid="'.$query_all[$i]['uuid'].'">刪除</button>';
                                }
                                ?>
                            </div>
                        </td>

                    </tr>
                    <?php
                }
                ?>

                </tbody>
            </table>
            <div class="pagination_links pagination_box">
                <?php echo $links; ?> </div>
        </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
<form  name="frm_01" id="frm_01" method="post">
    <input type="hidden" id="csrf" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
</form>
<style>
    .btn-group{
        width: 100%;
    }
    @media screen and (max-width: 767px){
        div.dt-buttons a.btn {
            float: left;
        }
    }

    input[type=checkbox] {
        transform: scale(1.2);
    }


    @media (max-width: @screen-xs) {
        .sort_number{font-size: 10px;}
    }

    @media (max-width: @screen-sm) {
        .sort_number{font-size: 14px;}
    }


    .sort_number{
        font-size: 1.4em;
    }
    .pagination_box{
        text-align: center;
    }
    .add_btn{
        float: right;
    }
</style>
<script>
    var ss_sort_name = '<?php echo isset( $_SESSION['ss_sort_name'] ) ?  $_SESSION['ss_sort_filed'] : '' ;?>';
    var ss_sort = '<?php echo isset( $_SESSION['ss_sort'] ) ?  $_SESSION['ss_sort']  : '' ;?>';

</script>
