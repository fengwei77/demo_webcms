<?php
/**
 * Created by PhpStorm.
 * User: fengw
 * Date: 2018/9/20
 * Time: 下午 04:15
 */
//取使用者權限
$CI =& get_instance();
$CI->load->model('basic_model');
$CI->load->model('admin_model');
$access_range = array();
$func_id = '';
if (isset($_SESSION['manager_data']))
{
    $uuid = $_SESSION['manager_data']['uuid'];
    $access_range = $CI->admin_model->get_access_range_id($uuid);
    $prefix_page_name = 'prefix_page_name_def';
    $search_filter = array('_sub', '_setting', '_content', '_qa');

    if (isset($_SESSION['ss_pagename']))
    {
        $prefix_page_name = str_replace($search_filter, '', $_SESSION['ss_pagename']);
    }
    foreach ($side_menu as $rows_0)
    {
        if (isset($rows_0['pagename']))
        {


            if ($rows_0['pagename'] == $prefix_page_name)
            {

                $func_id = $rows_0['id'];
                break;
            }
        }
        if ($rows_0['status'] == 0)
        {
            continue;
        }
        if (isset($rows_0['sub_menu']))
        {
            foreach ($rows_0['sub_menu'] as $rows_1)
            {
                if ($rows_1['pagename'] == $prefix_page_name)
                {
                    $func_id = $rows_1['id'];
                }


            }
        }
    }
} else
{
    redirect(base_url($CI->config->item('webcms_controllers_folder') . '/login'), 'location', 301);
}

$user_add = str_replace(" ", "", $func_id . '-add');
$user_mod = str_replace(" ", "", $func_id . '-mod');
$user_del = str_replace(" ", "", $func_id . '-del');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('webcms/home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><?php echo $unit_title ; ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Search box -->

    <form  name="frm_search" id="frm_search" method="get">
        <input type="hidden" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
        <div class="box">
            <div class="box-header with-border">

                <h4 class="box-title">搜尋操作</h4>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <div class="box-body">
                    <div class="col-lg-3 col-md-3 col-xs-3">
                        <input type="text" class="form-control" id="search_keywords" name="search_keywords" placeholder="輸入搜尋內容 ...">
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-3">
                        <select class="form-control search_status" id="search_status" name="search_status" >
                            <option value="">啟用狀態</option>
                            <option value="1">啟用</option>
                            <option value="0">關閉</option>
                        </select>
                    </div>

                    <div class="col-xs-1">
                        <button type="button" id="search_btn" class="btn btn-default search_btn">搜尋</button>

                    </div>
                    <div class="col-xs-1">
                        <button type="button" id="reset_btn" class="btn btn-default reset_btn">重置</button>

                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="box">
        <div class="box-body">
            <!-- /.box-header -->
            <div class="box-body">

                <div class="box-header">
                <small>
                    <?php
                    // echo $_SESSION['ss_page_size'];
                    // echo $prefix_page_name;
                    $page_size_arr = array(20,50,200);
                    $page_size = 20;
                    if(isset($_SESSION['ss_page_size_name']) == $prefix_page_name){
                        $page_size = $_SESSION['ss_page_size'];
                    }
                    ?>
                    <span class="col-lg-3 col-md-3 col-xs-3">
                    <select class="form-control"  id="page_size_select" name="page_size_select" style="width: 120px;" >
                        <?php
                        foreach ($page_size_arr as $row){
                            $show_row_num = $row . '筆';
//                            if($row > 100){
//                                $show_row_num = '全部';
//                            }
                            if($page_size == $row ){
                                echo '    <option value="'.$row.'" selected>顯示'.$show_row_num.'</option>';
                            }else{
                                echo '    <option value="'.$row.'">顯示'.$show_row_num.'</option>';

                            }
                        }

                        ?>
                    </select>
                        </span>
                    <span class="col-lg-9 col-md-9 col-xs-9">
                <?php
                if(in_array($user_add , $access_range)){
                    echo '  <button type="button" id="add_btn" class="btn btn-default add_btn" >新增項目</button>';
                }
                ?>
                    </span>
                </small>
                </div>
                <table id="data_list" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class="col-xs-2 col-md-2 col-lg-2">序號</th>
                        <th class="col-xs-3 col-md-3 col-lg-3">群組</th>
                        <th class="col-xs-3 col-md-3 col-lg-3">帳號</th>
                        <th class="col-xs-4 col-md-4 col-lg-4">操作</th>
                    </tr>
                    </thead>
                    <tbody id="data_msg" >
                    <tr>
                        <td colspan="3" align="center">Loading... </td>
                    </tr>
                    </tbody>
                    <tbody id="data_body" style="display: none;">
                    <?php
                        for($i = 0; $i <sizeof($query_all); $i++){

                    ?>
                    <tr class="row_<?php echo  $query_all[$i]['a_uuid']; ?>">
                        <td class="data_id"> <?php echo $page_num + $i + 1?> </td>
                        <td><?php echo ($query_all[$i]['b_name']) ; ?></td>
                        <td><?php echo  $CI->crypt->decryptdata($query_all[$i]['a_name'])  ; ?></td>
                        <td class="control_group">
                            <div class="btn-group">
                                <?php
                                if('系統管理員' !=  $CI->crypt->decryptdata($query_all[$i]['a_name']) )
                                {
                                    if (in_array($user_mod, $access_range))
                                    {
                                        echo '<input type="checkbox"  name="item_status" id="item_status-' . $query_all[$i]['a_id'] . '" class="margin  item_status" uuid="' . $query_all[$i]['a_uuid'] . '" ' . ($query_all[$i]['a_item_status'] == 1 ? 'checked' : '') . '>';

                                    } else
                                    {
                                        echo '<span class="bg-gray disabled color-palette" style="padding: 5px;width: 68px; margin-right: 5px;">';
                                        echo $query_all[$i]['a_item_status'] == 1 ? '啟用中 ' : '關閉中';
                                        echo '</span>';
                                    }
                                    if (in_array($user_mod, $access_range))
                                    {
                                        echo '<button type="button" class="btn-xs btn-warning modify_btn" uuid="' . $query_all[$i]['a_uuid'] . '">修改</button>';
                                    }
                                    if (in_array($user_del, $access_range))
                                    {
                                        echo '<button type="button" class="btn-xs btn-danger delete_btn" uuid="' . $query_all[$i]['a_uuid'] . '">刪除</button>';
                                    }
                                }else{
                                    if (in_array($user_mod, $access_range))
                                    {
                                        echo '<button type="button" class="btn-xs btn-primary modify_btn" style="width:70px;" uuid="' . $query_all[$i]['a_uuid'] . '">修改</button>';
                                    }
                                }
                                ?>
                            </div>
                    </tr>
                    <?php
                                  }
                    ?>

                    </tbody>
                </table>
                <div class="pagination_links pagination_box">
                    <?php echo $links; ?> </div>
               </div>
        </div>
            <!-- /.box-body -->
    </div>

    </div>
    <!-- /.box -->

</section>
<form  name="frm_01" id="frm_01" method="post">
    <input type="hidden" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
</form>
<!-- /.content -->
<style>
    .btn-group{
        width: 100%;
    }
    @media screen and (max-width: 767px){
        div.dt-buttons a.btn {
            float: left;
        }
    }
    .pagination_box{
        text-align: center;
    }
    .add_btn{
        float: right;
    }
</style>