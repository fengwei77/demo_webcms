<?php
//取使用者權限
$CI =& get_instance();
$CI->load->model('basic_model');
$CI->load->model('admin_model');

$access_range = array();
$func_id = '';
if(isset( $_SESSION['manager_data'])){
    $uuid =   $_SESSION['manager_data']['uuid'];
    $access_range = $CI->admin_model->get_access_range_id($uuid);

    $prefix_page_name = 'prefix_page_name_def';
    $search_filter  = array('_sub', '_setting','_content','_qa');

    if( isset($_SESSION['ss_pagename']) ){
        $prefix_page_name = str_replace( $search_filter,'',$_SESSION['ss_pagename']);
    }
    foreach ($side_menu as $rows_0)
    {
        if(isset($rows_0['pagename'])){


            if(  $rows_0['pagename']  ==  $prefix_page_name){

                $func_id = $rows_0['id'];
                break;
            }

        }
        if ($rows_0['status'] == 0) continue;
        if (isset($rows_0['sub_menu'])){
            foreach ($rows_0['sub_menu'] as $rows_1)
            {
                if(  $rows_1['pagename']  ==  $prefix_page_name){
                    $func_id = $rows_1['id'];
                }
            }
        }
    }

}else{
    redirect(base_url($CI->config->item('webcms_controllers_folder').'/login'), 'location', 301);
}

$user_add = str_replace(" ","", $func_id.'-add');
$user_mod = str_replace(" ","", $func_id.'-mod');
$user_del = str_replace(" ","", $func_id.'-del');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('webcms/home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><?php echo $unit_title ; ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">


    <form  name="frm_search" id="frm_search" method="get" style="display:  ;">
        <input type="hidden" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
        <div class="box">
            <div class="box-header with-border">

                <h4 class="box-title">搜尋操作</h4>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <div class="row" style="padding-bottom: 5px; display: none;">
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <input type="text" class="form-control" id="search_keywords" name="search_keywords" placeholder="輸入搜尋內容 ..." value="<?php echo $search_keywords == "" ?  "" : $search_keywords ; ?>" />
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <select class="form-control search_status" id="search_status" name="search_status" >
                                    <option value="" <?php echo $search_status == "" ? "selected" : "" ; ?>>啟用狀態</option>
                                    <option value="1" <?php echo $search_status == "1" ? "selected" : "" ; ?>>啟用</option>
                                    <option value="0" <?php echo $search_status == "0" ? "selected" : "" ; ?>>關閉</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2  col-sm-12 col-xs-12">
                                <div class="form-control " style="width: 140px; background-color: white;   left: 10px;padding-right: 5px;">
                                    <input type="checkbox" name="search_date" id="search_date" value="1" <?php echo $search_date == "1" ? "checked" : "" ; ?> >
                                    <label for="search_date">啟用時間範圍 :</label>
                                </div>
                            </div>
                            <div class="col-lg-10  col-md-10  col-sm-12 col-xs-12">
                                <table>
                                    <tr>
                                        <td style="margin-left: 0px; ">
                                                 <span class='input-group date' id='bs_start_date' style="width:auto; max-width: 200px;min-width: 100px;">
                                                <input type='text' class="form-control" name="start_date" id="start_date" value="<?php if(isset($start_date)){ echo $start_date;} ;?>" />
                                                <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                </span>
                                        </td>
                                        <td style="padding-right: 15px;padding-left: 15px;"><i class="fa fa-minus" aria-hidden="true"></i>
                                        </td>
                                        <td>
                                                   <span class='input-group date' id='bs_expire_date' style="width:auto; max-width: 200px;min-width: 100px;">
                                                <input type='text' class="form-control" name="expire_date" id="expire_date" value="<?php if(isset($expire_date)){ echo $expire_date;} ;?>" />
                                                <span class="input-group-addon">
                                                     <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                               </span>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12" style="padding-top: 10px;">
                                <button type="button" id="reset_btn" class="btn btn-default reset_btn">重置</button>

                                <button type="button" id="search_btn" class="btn btn-default search_btn">搜尋</button>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="box">
        <div class="box-body">
            <!-- /.box-header -->
            <div class="box-header">
                <small>
                    <?php
                    // echo $_SESSION['ss_page_size'];
                    // echo $prefix_page_name;
                    $page_size_arr = array(20,50,200);
                    $page_size = 20;
                    if(isset($_SESSION['ss_page_size_name']) == $prefix_page_name){
                        $page_size = $_SESSION['ss_page_size'];
                    }
                    ?>
                    <span class="col-lg-3 col-md-3 col-xs-3">
                    <select class="form-control"  id="page_size_select" name="page_size_select" style="width: 120px;" >
                        <?php
                        foreach ($page_size_arr as $row){
                            $show_row_num = $row . '筆';
                            if($page_size == $row ){
                                echo '    <option value="'.$row.'" selected>顯示'.$show_row_num.'</option>';
                            }else{
                                echo '    <option value="'.$row.'">顯示'.$show_row_num.'</option>';

                            }
                        }

                        ?>
                    </select>
                        </span>
                    <span class="col-lg-9 col-md-9 col-xs-9">
                        <a href="<?php echo base_url('webcms/fb/export') ; ?>" target="_blank"> <button type="button" id="export_btn" class="btn btn-success export_btn" ><i class="fa fa-file-excel-o" aria-hidden="true"></i>  匯出</button></a>
                        </span>
                </small>
            </div>
            <table id="data_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th class="col-lg-2  col-sm-2 col-xs-2">id</th>
                    <th class="col-lg-2  col-sm-2 col-xs-2">名稱</th>
                    <th class="col-lg-2  col-sm-2 col-xs-2">email</th>
                    <th class="col-lg-6  col-sm-6 col-xs-6 do_sort"  id="create_datetime"  sort="">建立時間 <i class="fa fa-sort" aria-hidden="true"></i></th>
                 </tr>
                </thead>
                <tbody id="data_msg" >
                <tr>
                    <td colspan="8" align="center">Loading... </td>
                </tr>
                </tbody>
                <tbody id="data_body" style="display: none;">
                <?php
                for($i = 0; $i <sizeof($query_all); $i++){
                    ?>
                    <tr class="row_<?php echo  $query_all[$i]['id']; ?>">
                        <td><?php echo $query_all[$i]['fb_id'] ; ?> </td>
                        <td><?php echo $query_all[$i]['fb_name'] ; ?> </td>
                        <td><?php echo $query_all[$i]['fb_email'] == '' ? '<span style="color: #aeaeae">使用者未提供</span>': $query_all[$i]['fb_email']; ?> </td>
                        <td><?php echo $query_all[$i]['create_datetime'] ; ?> </td>
                    </tr>
                    <?php
                }
                ?>

                </tbody>
            </table>
            <div class="pagination_links pagination_box">
                <?php echo $links; ?> </div>
        </div>
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
<form  name="frm_01" id="frm_01" method="post">
    <input type="hidden" id="csrf" name="<?php echo $csrf_name ;?>" value="<?php echo $csrf_hash ;?>" />
</form>
<style>
    .btn-group{
        width: 100%;
    }
    @media screen and (max-width: 767px){
        div.dt-buttons a.btn {
            float: left;
        }
    }


    input[type=checkbox] {
        /*transform: scale(1.2);*/
    }


    @media (max-width: @screen-xs) {
        .order_num{font-size: 10px;}
    }

    @media (max-width: @screen-sm) {
        .order_num{font-size: 14px;}
    }


    .order_num{
        font-size: 1.4em;
    }

    .btn-group{
        width: 100%;
    }
    @media screen and (max-width: 767px){
        div.dt-buttons a.btn {
            float: left;
        }
    }
    input[type=checkbox] {
        transform: scale(1.2);
    }
    .pagination_box{
        text-align: center;
    }
    .search_btn{
        float: left;
        margin-right: 5px;
    }
    .reset_btn{
        float: left;
        margin-right: 5px;
    }
    .export_btn{
        float: right;
        margin-right: 5px;
    }

    .datepicker > div {
        display: block;
    }
</style>

<script>
    var ss_sort_name = '<?php echo isset( $_SESSION['ss_sort_name'] ) ?  $_SESSION['ss_sort_filed'] : '' ;?>';
    var ss_sort = '<?php echo isset( $_SESSION['ss_sort'] ) ?  $_SESSION['ss_sort']  : '' ;?>';

</script>
